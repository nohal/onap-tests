#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
import logging
import json
import requests

import onap_tests.utils.utils as onap_utils
import onap_tests.utils.exceptions as onap_test_exceptions

PROXY = onap_utils.get_config("general.proxy")
SDC_URL = onap_utils.get_config("onap.sdc.url")
SDC_URL2 = onap_utils.get_config("onap.sdc.url2")
SDC_HEADERS = onap_utils.get_config("onap.sdc.headers")
SDC_HEADERS_DESIGNER = SDC_HEADERS.copy()
SDC_HEADERS_DESIGNER["USER_ID"] = "cs0008"


class Vendor():
    """
        ONAP Vendor Object
        Used for SDC operations
    """
    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        # pylint: disable=invalid-name
        self.id = ""
        if "vendor_name" in kwargs:
            self.name = kwargs['vendor_name']
        else:
            self.name = "generic"
        self.version = ""
        self.status = ""

    def update_vendor(self, **kwargs):
        """
            Update vendor values
        """
        if "id" in kwargs:
            self.id = kwargs['id']
        if "name" in kwargs:
            self.name = kwargs['name']
        if "version" in kwargs:
            self.version = kwargs['version']
        if "status" in kwargs:
            self.status = kwargs['status']

    def get_sdc_vendor_payload(self, **kwargs):
        """
            Build SDC vendor payload
        """
        try:
            payload = {}
            if kwargs['action'] == "Create":
                payload = onap_utils.get_config(
                    "vlm_payload.vlm_create_data")
                if "vendor_name" in kwargs:
                    payload['vendorName'] = kwargs['vendor_name']
                else:
                    payload['vendorName'] = "Generic-Vendor"
            # if kwargs['action'] == "Checkin":
            #     payload = onap_utils.get_config(
            #         "vlm_payload.vlm_checkin_data")
            if kwargs['action'] == "Submit":
                payload = onap_utils.get_config(
                    "vlm_payload.vlm_submit_data")
        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_vendor_list(self):
        """
            Get SDC vendor list
        """
        # now it is time to request data to ONAP SDC
        url = SDC_URL2 + onap_utils.get_config(
            "onap.sdc.list_vlm_url")
        vendor_list = {}
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            vendor_list = response.json()
            self.__logger.info(
                "Get vendor list code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to get vendor list: %s", err)
            raise onap_test_exceptions.VendorNotFoundException
        return vendor_list

    def check_vendor_exists(self):
        """
            Check if provided vendor exists in SDC vendor list
        """
        vendor_list = self.get_vendor_list()

        vendor_found = False
        for result in vendor_list['results']:
            if result['name'] == self.name:
                self.__logger.info("Vendor found")
                self.update_vendor(id=result['id'])
                vendor_found = True
                old_str = onap_utils.get_config(
                    "onap.sdc.get_vlm_status_url")
                new_str = old_str.replace("PPvlm_idPP", self.id)
                url = SDC_URL2 + new_str
                headers = SDC_HEADERS_DESIGNER
                response = requests.get(url, headers=headers,
                                        proxies=PROXY, verify=False)
                if response.status_code == 200:
                    get_status_response = response.json()
                    self.update_vendor(
                        status=get_status_response['results'][0]['status'])
                    self.update_vendor(
                        version=get_status_response['results'][0]['id'])
                    self.__logger.info(
                        "Get vendor status code : %s", response.status_code)
                else:
                    self.__logger.error(
                        "Get vendor status code : %s", response.status_code)
        return vendor_found

    def create_vendor(self):
        """
            Create vendor in SDC (only if he/she does not exist)
        """
        # we check if vendor exists or not, if not we create it
        create_vendor = False
        if not self.check_vendor_exists():
            try:
                url = SDC_URL2 + onap_utils.get_config(
                    "onap.sdc.create_vlm_url")
                data = self.get_sdc_vendor_payload(action="Create",
                                                   vendor_name=self.name)
                data = json.dumps(data)
                headers = SDC_HEADERS_DESIGNER
                self.__logger.info(
                    "Create vendor url : %s", url)
                self.__logger.info(
                    "Create vendor payload : %s", data)
                response = requests.post(url, headers=headers,
                                         proxies=PROXY, verify=False,
                                         data=data)
                if response.status_code == 200:
                    create_vendor = True
                    create_response = response.json()
                    self.update_vendor(
                        status=create_response['version']['status'],
                        id=create_response['itemId'],
                        version=create_response['version']['id'])
                    self.__logger.info(
                        "Create vendor code : %s", response.status_code)
                    self.check_vendor_exists()
                else:
                    self.__logger.error(
                        "Create vendor code : %s", response.status_code)
            except Exception as err:  # pylint: disable=broad-except
                self.__logger.error("Failed to create vendor: %s", err)
        return create_vendor

    def get_vendor_info_from_sdc(self):
        """
            Get vendor detail
        """
        vendor_list = self.get_vendor_list()

        for result in vendor_list['results']:
            if result['name'] == self.name:
                return result
        return None

    # def checkin_vendor(self):
    #     """
    #         Checkin SDC Vendor
    #     """
    #     checkin_vendor = False
    #     old_str = onap_utils.get_config("onap.sdc.checkin_vlm_url")
    #     new_str = old_str.replace("PPvlm_idPP", self.id)
    #     old_str = new_str
    #     new_str = old_str.replace("PPvlm_versionPP", self.version)
    #     url = SDC_URL2 + new_str
    #     data = self.get_sdc_vendor_payload(action="Checkin")
    #     data = json.dumps(data)
    #     headers = SDC_HEADERS_DESIGNER
    #     try:
    #         response = requests.put(url,
    #                                 headers=headers,
    #                                 proxies=PROXY,
    #                                 verify=False,
    #                                 data=data)
    #         if response.status_code == 200:
    #             checkin_vendor = True
    #             self.__logger.info(
    #                 "Checkin vendor code : %s", response.status_code)
    #         else:
    #             self.__logger.error(
    #                 "Checkin vendor code : %s", response.status_code)
    #     except Exception as err:  # pylint: disable=broad-except
    #         self.__logger.error("Failed to perform checkin\
    #             SDC vendor: %s", err)
    #     return checkin_vendor

    def submit_vendor(self):
        """
            Submit SDC Vendor
        """
        submit_vendor = False
        old_str = onap_utils.get_config("onap.sdc.submit_vlm_url")
        new_str = old_str.replace("PPvlm_idPP", self.id)
        old_str = new_str
        new_str = old_str.replace("PPvlm_versionPP", self.version)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vendor_payload(action="Submit")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.put(url,
                                    headers=headers,
                                    proxies=PROXY, verify=False,
                                    data=data)
            if response.status_code == 200:
                submit_vendor = True
                self.__logger.info(
                    "Submit vendor code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Submit vendor code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform SDC vendor submit: %s",
                                err)
        return submit_vendor
