#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
# pylint: disable=too-many-instance-attributes
#
"""
   ONAP NBI main operations
"""
import datetime
import json
import logging
import time

import requests

import onap_tests.utils.exceptions as onap_test_exceptions
import onap_tests.utils.utils as onap_test_utils

PROXY = onap_test_utils.get_config("general.proxy")
NBI_HEADERS = onap_test_utils.get_config("onap.nbi.headers")
NBI_URL = onap_test_utils.get_config("onap.nbi.url")


class Nbi():
    """
        ONAP NBI main operations
    """
    __logger = logging.getLogger(__name__)

    def __init__(self, service_name, service_instance_name):
        # Logger
        # pylint: disable=no-member
        requests.packages.urllib3.disable_warnings()
        self.service_name = service_name
        self.service_instance_name = service_instance_name
        self.customer = onap_test_utils.get_config("onap.customer")
        self.external_id = "BSS_" + service_instance_name
        self.catalog_id = ""

    def get_request_info(self):
        """
            Get NBI Request Info
        """
        return {
            "instanceName": self.service_instance_name,
            "source": "NBI",
            "suppressRollback": False,
            "productFamilyId": self.service_instance_name,
            "requestorId": "NBI"
        }

    def get_service_order_payload(self):
        """
            Get SO Service paylod
        """
        format_iso_now = datetime.datetime.now().isoformat()[:-3] + "Z"
        return json.dumps({
            "externalId": self.external_id,
            "priority": "1",
            "description": "Service Order",
            "category": "Consumer",
            "requestedStartDate": format_iso_now,
            "requestedCompletionDate": format_iso_now,
            "orderItem": [{
                "id": "1",
                "action": "add",
                "service": {
                    "name": self.service_instance_name,
                    "serviceState": "active",
                    "serviceSpecification": {
                        "id": self.catalog_id}}}]
        })

    def get_instance_id_from_order(self, service_order_id):
        """
            Get NBI Service Catalog
        """
        url = NBI_URL + "/serviceOrder/" + service_order_id
        self.__logger.debug("NBI request: %s", url)
        service_instance_found = False
        instance_id = None
        nb_try = 0
        nb_try_max = 5
        while service_instance_found is False and nb_try < nb_try_max:
            response = onap_test_utils.get_simple_request(
                url, NBI_HEADERS, PROXY)
            self.__logger.debug("NBI response: %s", response)
            self.__logger.info("NBI: looking for service instance id....")
            if response["state"] == "COMPLETED":
                self.__logger.info("NBI request COMPLETED")
                service_instance_found = True
                instance_id = response["orderItem"][0]["service"]["id"]
                self.__logger.info("Instance ID found %s", instance_id)
            time.sleep(10)
            nb_try += 1
        if service_instance_found is False:
            self.__logger.info("Service instance not found")
            raise onap_test_exceptions.NbiException()
        return instance_id

    def create_service_order_nbi(self):
        """
            NBI create servbice order
        """
        instance_id = None
        # Check that the service name exists in the NBI catalog
        nbi_catalog = self.get_nbi_service_catalog()

        # If it does not exxit raise an execption
        service_found = False
        for nbi_service in nbi_catalog:
            if nbi_service['name'] == self.service_name:
                service_found = True
                self.catalog_id = nbi_service['id']
                self.__logger.debug("Service name %s found in NBI catalog",
                                    self.service_name)
                self.__logger.debug("NBI catalog ID: %s",
                                    self.catalog_id)
        if not service_found:
            raise onap_test_exceptions.NbiException()

        # else create the service order
        url = NBI_URL + "/serviceOrder"
        self.__logger.debug("NBI request: %s", url)
        response = requests.post(url, headers=NBI_HEADERS,
                                 proxies=PROXY, verify=False,
                                 data=self.get_service_order_payload())
        self.__logger.info("NBI create service order payload: %s",
                           self.get_service_order_payload())
        self.__logger.info("NBI create service order request: %s",
                           response.text)
        instance_id = response.json()
        return instance_id

    def get_nbi_service_order(self, **kwargs):
        """
            Get NBI Service order
            If an order_id is precised as kwargs, retrieve only related info
        """
        url = NBI_URL + "/serviceOrder"
        if "order_id" in kwargs:
            url += "/" + kwargs["order_id"]
        self.__logger.debug("NBI request: %s", url)
        return onap_test_utils.get_simple_request(url,
                                                  NBI_HEADERS,
                                                  PROXY)

    def get_nbi_service_catalog(self):
        """
            Get NBI Service Catalog
        """
        url = NBI_URL + "/serviceSpecification"
        self.__logger.debug("NBI request: %s", url)
        return onap_test_utils.get_simple_request(url,
                                                  NBI_HEADERS,
                                                  PROXY)

    def get_nbi_service_inventory(self):
        """
            Get NBI service inventory
        """
        url = NBI_URL + "/service"
        self.__logger.debug("NBI request: %s", url)
        return onap_test_utils.get_simple_request(url,
                                                  NBI_HEADERS,
                                                  PROXY)
