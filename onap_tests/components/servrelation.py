#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=too-many-branches


"""Class to define Service onboarding"""

import os
import fnmatch
import logging
import urllib3

import onap_tests.utils.utils as onap_utils
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)



class Servrelation():
    """
        ONAP Service Object
        Used for SDC operations
    """
    __logger = logging.getLogger(__name__)

    def __init__(self):
        pass

    def get_sdc_service_payload(self, **kwargs):
        """
            Build SDC service payload
        """
        try:
            payload = {}

            if kwargs['action'] == "create_relation_between_vf_url":
                payload = onap_utils.get_config(
                    "service_payload.service_associate_vfs")
            if kwargs['action'] == "create_port_relation_between_vf_url":
                payload = onap_utils.get_config(
                    "service_payload.service_associate_vfs_viaport")
            if kwargs['action'] == "create_attachment_relation_between_vf_url":
                payload = onap_utils.get_config(
                    "service_payload.service_associate_vfs_viaattachment")

        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_default_vnfname(self, vflink_name):
        """
            Get default vnf name from env heat file
        """
        get_default_vnfname = ""
        envdir = os.path.dirname(os.path.abspath(__file__))
        envdir = envdir.replace("components",
                                "templates/heat_files/" +
                                vflink_name)
        for entry in os.listdir(envdir):
            if fnmatch.fnmatch(entry, "*.env"):
                envfile = entry
        heatparameters = onap_utils.get_parameter_from_yaml(
            "parameters",
            envdir + "/" + envfile)
        get_default_vnfname = "feature_" + heatparameters["vnf_name"]
        self.__logger.debug("Get default vnf name from heat files: %s",\
            get_default_vnfname)
        return get_default_vnfname

    def set_featurepayload_relation(self,
                                    data,
                                    vf_name,
                                    vflink_name):
        """
            Set payload service information for relation with feature type
        """
        fname = self.get_default_vnfname(vflink_name)
        payload = self.get_sdc_service_payload(
            action="create_relation_between_vf_url")
        payload['relationships'][0]["relation"]["capability"] = fname
        payload['relationships'][0]["relation"]["requirement"] = \
            vf_name
        for result in data['componentInstances']:
            self.__logger.debug(\
                "Component Name : %s", result['componentName'])
            if result['componentName'] == vf_name:
                payload['fromNode'] = result['uniqueId']
                for cap in \
                    result['requirements']['tosca.capabilities.Node']:
                    self.__logger.debug(\
                        "Cap feature Name : %s", cap['name'])
                    if cap['name'] == vf_name:
                        payload['relationships'][0]["relation"]\
                            ["requirementUid"] = cap['uniqueId']
                        payload['relationships'][0]["relation"]\
                            ["requirementOwnerId"] = cap['ownerId']
            if result['componentName'] == vflink_name:
                payload['toNode'] = result['uniqueId']
                for cap in \
                    result['capabilities']['tosca.capabilities.Node']:
                    self.__logger.debug(\
                        "Cap Name : %s", cap['name'])
                    if cap['name'] == fname:
                        payload['relationships'][0]["relation"]\
                            ["capabilityUid"] = cap['uniqueId']
                        payload['relationships'][0]["relation"]\
                            ["capabilityOwnerId"] = cap['ownerId']
        return payload

    def set_portpayload_relation(self,
                                 data,
                                 vf_name,
                                 vflink_name):
        """
            Set payload service information for relation with port type
        """
        payload = self.get_sdc_service_payload(
            action="create_port_relation_between_vf_url")
        payload['relationships'][0]["relation"]["capability"] = \
            vf_name
        for result in data['componentInstances']:
            self.__logger.debug("Component Name port link : %s",
                                result['componentName'])
            if result['componentName'] == vf_name:
                payload['toNode'] = result['uniqueId']
                for cap in \
                    result['capabilities']\
                    ['tosca.capabilities.network.Linkable']:
                    self.__logger.debug(\
                        "Cap Name : %s", cap['name'])
                    if cap['name'] == vf_name:
                        payload['relationships'][0]["relation"]\
                            ["capabilityUid"] = cap['uniqueId']
                        payload['relationships'][0]["relation"]\
                            ["capabilityOwnerId"] = cap['ownerId']
            if result['componentName'] == vflink_name:
                payload['fromNode'] = result['uniqueId']
                for cap in \
                    result['requirements']\
                    ['tosca.capabilities.network.Linkable']:
                    self.__logger.debug(\
                        "Cap Name : %s", cap['name'])
                    payload['relationships'][0]["relation"]\
                            ["requirement"] = cap['name']
                    payload['relationships'][0]["relation"]\
                            ["requirementOwnerId"] = cap['ownerId']
                    payload['relationships'][0]["relation"]\
                            ["requirementUid"] = cap['uniqueId']
        return payload

    def set_attachmentpayload_relation(self,
                                       data,
                                       vf_name,
                                       vflink_name):
        """
            Set payload service information for relation with attachment type
        """
        payload = self.get_sdc_service_payload(
            action="create_attachment_relation_between_vf_url")
        for result in data['componentInstances']:
            self.__logger.debug("Component Name volume link : %s",
                                result['componentName'])
            if result['componentName'] == vflink_name:
                payload['toNode'] = result['uniqueId']
                for cap in \
                    result['capabilities']\
                    ['tosca.capabilities.Attachment']:
                    self.__logger.debug(\
                        "Cap Name : %s", cap['name'])
                    if cap['name'] == payload['relationships'][0]["relation"]\
                        ["capability"]:
                        payload['relationships'][0]["relation"]\
                            ["capabilityUid"] = cap['uniqueId']
                        payload['relationships'][0]["relation"]\
                            ["capabilityOwnerId"] = cap['ownerId']
            if result['componentName'] == vf_name:
                payload['fromNode'] = result['uniqueId']
                for cap in \
                    result['requirements']\
                    ['tosca.capabilities.Attachment']:
                    self.__logger.debug(\
                        "Cap Name : %s", cap['name'])
                    payload['relationships'][0]["relation"]\
                            ["requirement"] = cap['name']
                    payload['relationships'][0]["relation"]\
                            ["requirementOwnerId"] = cap['ownerId']
                    payload['relationships'][0]["relation"]\
                            ["requirementUid"] = cap['uniqueId']
        return payload

    def set_payload_relation(self,
                             data,
                             vf_name,
                             vflink_name,
                             relation_type):
        """
            Set payload service information for VFs relations
        """
        self.__logger.debug("Set Payload configrelation")
        self.__logger.debug("From VF name : %s", vf_name)
        self.__logger.debug("To VF name : %s", vflink_name)

        if relation_type == "feature":
            payload = self.set_featurepayload_relation(data,
                                                       vf_name,
                                                       vflink_name)
        if relation_type == "port":
            payload = self.set_portpayload_relation(data,
                                                    vf_name,
                                                    vflink_name)
        if relation_type == "attachment":
            payload = self.set_attachmentpayload_relation(data,
                                                          vf_name,
                                                          vflink_name)
        return payload
