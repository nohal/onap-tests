#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""
   ONAP AA&I main operations
"""
import logging
import time
import requests

import onap_tests.utils.exceptions as onap_test_exceptions
import onap_tests.utils.utils as onap_test_utils

PROXY = onap_test_utils.get_config("general.proxy")
AAI_HEADERS = onap_test_utils.get_config("onap.aai.headers")
AAI_URL = onap_test_utils.get_config("onap.aai.url")


class Aai():
    """
        ONAP AA&I main operations
    """
    __logger = logging.getLogger(__name__)

    def __init__(self):
        # pylint: disable=no-member
        requests.packages.urllib3.disable_warnings()

    def check_service_instance(self, service_description, service_instance_id):
        """
        Check that a given service instance is created
        send request, wait, check
        max nb_try_max times
        """
        url = (AAI_URL + "/aai/v11/business/customers/customer/" +
               onap_test_utils.get_config("onap.customer") +
               "/service-subscriptions/service-subscription/" +
               service_description + "/service-instances/")
        try:
            service_instance_found = False
            nb_try = 0
            nb_try_max = 5
            while service_instance_found is False and nb_try < nb_try_max:
                response = requests.get(url, headers=AAI_HEADERS,
                                        proxies=PROXY, verify=False)
                self.__logger.debug("AAI: looking for %s service instance....",
                                    service_instance_id)
                if service_instance_id in response.text:
                    self.__logger.info("Service instance %s found in AAI",
                                       service_instance_id)
                    service_instance_found = True
                time.sleep(10)
                nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if service_instance_found is False:
            self.__logger.info("Service instance not found")
            raise onap_test_exceptions.ServiceInstantiateException
        return service_instance_found

    def check_service_instance_cleaned(self, service_description,
                                       service_instance_id):
        """
        Check if the Service instance has been  cleaned in the AAI
        return True if it has been clean
        return False elsewhere
        """
        # url2 = AAI_URL + "/aai/v11/service-design-and-creation/services"
        # url1 = AAI_URL + "/aai/v11/business/customers"
        url = (AAI_URL + "/aai/v11/business/customers/customer/" +
               onap_test_utils.get_config("onap.customer") +
               "/service-subscriptions/service-subscription/" +
               service_description + "/service-instances/")
        try:
            service_instance_found = True
            nb_try = 0
            nb_try_max = 5
            while service_instance_found is True and nb_try < nb_try_max:
                response = requests.get(url, headers=AAI_HEADERS,
                                        proxies=PROXY, verify=False)
                self.__logger.debug("AAI: %s service instance cleaned?",
                                    service_instance_id)
                if service_instance_id not in response.text:
                    self.__logger.info("Service instance %s cleaned in AAI",
                                       service_instance_id)
                    service_instance_found = False
                time.sleep(10)
                nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if service_instance_found is False:
            self.__logger.info("Service instance cleaned")
        else:
            raise onap_test_exceptions.ServiceInstantiateException

        return not service_instance_found

    def check_vnf_instance(self, vnf_id):
        """
            Check the VNF declared in the AA&I
        """
        url = (AAI_URL + "/aai/v11/network/generic-vnfs")
        try:
            vnf_instance_found = False
            nb_try = 0
            nb_try_max = 5
            while vnf_instance_found is False and nb_try < nb_try_max:
                response = requests.get(url, headers=AAI_HEADERS,
                                        proxies=PROXY, verify=False)
                self.__logger.debug("AAI: looking for %s vnf instance....",
                                    vnf_id)
                if vnf_id in response.text:
                    self.__logger.info("Vnf instance %s found in AAI",
                                       vnf_id)
                    vnf_instance_found = True
                time.sleep(10)
                nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if vnf_instance_found is False:
            self.__logger.info("VNF instance not found")
            raise onap_test_exceptions.VNFInstantiateException

        return vnf_instance_found

    def check_vnf_instances(self, vnf_ids):
        """
            Check the VNF(s) declared in the AA&I
        """
        url = (AAI_URL + "/aai/v11/network/generic-vnfs")
        # if case of several VNFs, answer False if one is not found
        nb_vnf_found = 0
        for elt in vnf_ids:
            try:
                self.__logger.info("AAI: looking for %s vnf instance....", elt)
                vnf_id = vnf_ids[elt]["id"]
                nb_try = 0
                nb_try_max = 5
                vnf_instance_found = False
                while vnf_instance_found is False and nb_try < nb_try_max:
                    response = requests.get(url, headers=AAI_HEADERS,
                                            proxies=PROXY, verify=False)
                    if vnf_id in response.text:
                        self.__logger.info("Vnf instance %s found in AAI",
                                           vnf_id)
                        nb_vnf_found += 1
                        vnf_instance_found = True
                    time.sleep(10)
                    nb_try += 1
            except Exception as err:  # pylint: disable=broad-except
                self.__logger.error("Error on AAI request: %s",
                                    str(err))
            if nb_vnf_found == len(vnf_ids):
                self.__logger.info("VNF instance found")
                vnf_instance_found = True
            else:
                vnf_instance_found = False
                self.__logger.error("VNF not found")
                raise onap_test_exceptions.VNFInstantiateException

        return vnf_instance_found

    def check_vnf_cleaned(self, vnf_id):
        """
            Check the VNF declared in the AA&I
        """
        url = AAI_URL + "/aai/v11/network/generic-vnfs"
        try:
            vnf_instance_found = True
            nb_try = 0
            nb_try_max = 5
            while vnf_instance_found is True and nb_try < nb_try_max:
                response = requests.get(url, headers=AAI_HEADERS,
                                        proxies=PROXY, verify=False)
                self.__logger.info("AAI: check if vnf %s instance is cleaned.",
                                   vnf_id)
                if vnf_id not in response.text:
                    self.__logger.info("Vnf instance %s cleaned in AAI",
                                       vnf_id)
                    vnf_instance_found = False
                time.sleep(10)
                nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if vnf_instance_found is True:
            self.__logger.info("VNF still in AAI, instance not cleaned")
        return not vnf_instance_found

    def check_module_instance(self, vnf_id, module_id):
        """
            Check the VNF declared in the AA&I
        """
        url = (AAI_URL + "/aai/v11/network/generic-vnfs/generic-vnf/" +
               vnf_id + "/vf-modules")
        try:
            module_instance_found = False
            nb_try = 0
            nb_try_max = 20
            while module_instance_found is False and nb_try < nb_try_max:
                response = requests.get(url, headers=AAI_HEADERS,
                                        proxies=PROXY, verify=False)
                self.__logger.info("AAI: looking for %s module instance....",
                                   module_id)
                if module_id in response.text:
                    self.__logger.info("Module instance %s found in AAI",
                                       module_id)
                    module_instance_found = True
                time.sleep(20)
                nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if module_instance_found is False:
            self.__logger.info("VFModule instance not found")
            raise onap_test_exceptions.VfModuleInstantiateException
        return module_instance_found

    def check_module_cleaned(self, vnf_id, module_id):
        """
            Check that the VfModule declared in the AA&I has been cleaned
            return True if it has been cleaned
            return False is not
        """
        url = (AAI_URL + "/aai/v11/network/generic-vnfs/generic-vnf/" +
               vnf_id + "/vf-modules")
        try:
            module_instance_found = True
            nb_try = 0
            nb_try_max = 10
            while module_instance_found is True and nb_try < nb_try_max:
                response = requests.get(url, headers=AAI_HEADERS,
                                        proxies=PROXY, verify=False)
                self.__logger.info("AAI: Check if module  %s has been cleaned",
                                   module_id)
                if module_id not in response.text:
                    self.__logger.info("Module instance %s cleaned in AAI",
                                       module_id)
                    module_instance_found = False
                time.sleep(15)
                nb_try += 1
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("impossible to perform the request on AAI: %s",
                                str(err))
        if module_instance_found is True:
            self.__logger.info("VFModule still in AAI, instance not cleaned")
        return not module_instance_found

    @classmethod
    def get_customers(cls):
        """
            Get the list of subscription types in AAI
        """
        url = AAI_URL + "/aai/v13//business/customers"
        return onap_test_utils.get_simple_request(url,
                                                  AAI_HEADERS,
                                                  PROXY)

    @classmethod
    def get_subscription_type_list(cls):
        """
            Get the list of subscription types in AAI
        """
        url = AAI_URL + "/aai/v13/service-design-and-creation/services"
        return onap_test_utils.get_simple_request(url,
                                                  AAI_HEADERS,
                                                  PROXY)

    @classmethod
    def get_vnfid_from_instance_id(cls, instance_id):
        """
            Get the VNF_ID associated with an instance-id
        """
        url = (AAI_URL + "/aai/v13/nodes/service-instances/service-instance/" +
               instance_id)
        return onap_test_utils.get_simple_request(url,
                                                  AAI_HEADERS,
                                                  PROXY)

    @classmethod
    def get_vfmoduleid_from_vnf_id(cls, vnf_id):
        """
            Get the list of subscription types in AAI
        """
        url = (AAI_URL + "/aai/v13/network/generic-vnfs/generic-vnf/" +
               vnf_id + "/vf-modules")
        return onap_test_utils.get_simple_request(url,
                                                  AAI_HEADERS,
                                                  PROXY)

    @classmethod
    def get_infos_from_instance_id(cls, customer_id,
                                   service_type, instance_id):
        """
            Get the list of subscription types in AAI
        """
        url = (AAI_URL + "/aai/v13/business/customers/customer/" +
               customer_id + "/service-subscriptions/service-subscription/" +
               service_type + "/service-instances/service-instance/" +
               instance_id)
        return onap_test_utils.get_simple_request(url,
                                                  AAI_HEADERS,
                                                  PROXY)

    @classmethod
    def put_custom_query(cls, instance_id, query):
        """
            Get a module from an instance id
        """
        url = AAI_URL + "/aai/v13/query?format=simple"
        start = ["business/" + instance_id]
        # "query" : "query/vfModule-fromServiceInstance"}
        data = {"start": start,
                "query": query}
        return requests.put(url,
                            headers=AAI_HEADERS,
                            proxies=PROXY,
                            verify=False,
                            data=data)

    @classmethod
    def get_cloud_regions(cls):
        """
            Get the list of subscription types in AAI
        """
        url = AAI_URL + "/cloud-infrastructure/cloud-regions"
        return onap_test_utils.get_simple_request(url,
                                                  AAI_HEADERS,
                                                  PROXY)
