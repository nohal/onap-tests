#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
import logging
import json
from copy import deepcopy
import base64
import os
import hashlib
import requests
import onap_tests.utils.exceptions as onap_test_exceptions
import onap_tests.utils.utils as onap_utils

PROXY = onap_utils.get_config("general.proxy")
SDC_URL = onap_utils.get_config("onap.sdc.url")
SDC_URL2 = onap_utils.get_config("onap.sdc.url2")
SDC_HEADERS = onap_utils.get_config("onap.sdc.headers")
SDC_HEADERS_DESIGNER = SDC_HEADERS.copy()
SDC_HEADERS_DESIGNER["USER_ID"] = "cs0008"
SDC_HEADERS_TESTER = SDC_HEADERS.copy()
SDC_HEADERS_TESTER["USER_ID"] = "jm0007"
SDC_UPLOAD_HEADERS = onap_utils.get_config("onap.sdc.upload_headers")
SDC_UPLOAD_HEADERS_VFC = onap_utils.get_config("onap.sdc.vfc_upload_headers")


class VFC():
    """
        ONAP VFC Object
        Used for SDC operations
    """
    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        # pylint: disable=invalid-name
        self.id = ""
        if "vfc_name" in kwargs:
            self.name = kwargs['vfc_name']
        else:
            self.name = ""
        self.version = ""
        self.status = ""
        self.vendor_name = ""
        self.unique_id = ""
        if "service_name" in kwargs:
            self.service_name = kwargs['service_name']
        else:
            self.name = ""


    def update_vfc(self, **kwargs):
        """
            Update vendor values
        """
        if "id" in kwargs:
            self.id = kwargs['id']
        if "unique_id" in kwargs:
            self.unique_id = kwargs['unique_id']
        if "name" in kwargs:
            self.name = kwargs['name']
        if "service_name" in kwargs:
            self.service_name = kwargs['service_name']
        if "version" in kwargs:
            self.version = kwargs['version']
        if "status" in kwargs:
            self.status = kwargs['status']
        if "vendor_name" in kwargs:
            self.vendor_name = kwargs['vendor_name']

    def get_sdc_vfc_payload(self, **kwargs):
        """
            Build SDC vfc payload
        """
        try:
            payload = {}
            if kwargs['action'] == "Create":
                payload = onap_utils.get_config(
                    "vfc_payload.vfc_create_data")
                if "vfc_name" in kwargs:
                    payload['name'] = kwargs['vfc_name']
                else:
                    payload['name'] = "ONAP-test-VFC"
            if kwargs['action'] == "Checkin":
                payload = onap_utils.get_config(
                    "vfc_payload.vfc_checkin_data")
            if kwargs['action'] == "Submit_for_testing":
                payload = onap_utils.get_config(
                    "vfc_payload.vfc_submit_for_testing_data")
            if kwargs['action'] == "Start_certification":
                payload = onap_utils.get_config(
                    "vfc_payload.vfc_start_certification_data")
            if kwargs['action'] == "Certify":
                payload = onap_utils.get_config("vfc_payload.vfc_certify_data")
            if kwargs['action'] == "NewVersion":
                payload = onap_utils.get_config(
                    "vfc_payload.new_vfc_version_data")
        except KeyError:
            self.__logger.error("No vfc payload set")
        return payload

    def get_vfc_list(self):
        """
            Get vfc list
        """
        url = SDC_URL + onap_utils.get_config(
            "onap.sdc.list_vfc_url")
        vfc_list = {}
        try:
            headers = SDC_HEADERS_DESIGNER
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            vfc_list = response.json()
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to get vfc list: %s", err)
            raise onap_test_exceptions.VfcNotFoundException
        return vfc_list

    def get_last_version(self, vfc_versions):
        highest_version = {}
        list_version = []
        for elem in vfc_versions['results']:
            list_version.append(int(elem['name'][:-2]))
        max_version = max(list_version)
        for elem in vfc_versions['results']:
            if int(elem['name'][:-2]) == int(max_version):
                highest_version = deepcopy(elem)
        self.update_vfc(status=highest_version['status'])
        self.update_vfc(version=highest_version['id'])
        self.__logger.debug("VERSION : %s", self.version)

    def check_vfc_exists(self):
        """
            Check if provided vfc exists in vfc list
        """
        vfc_list = self.get_vfc_list()
        vfc_found = False
        for result in vfc_list:
            if result['name'] == self.name:
                vfc_found = True
                self.__logger.info("VFC %s found in VFC list", self.name)
                self.update_vfc(id=result["uuid"])
                self.update_vfc(version=result["version"])
                self.update_vfc(status=result["lifecycleState"])
        if not vfc_found:
            self.__logger.info("VF in not in vf list: %s", self.name)
            self.update_vfc(id="")
            self.update_vfc(unique_id="")
            self.update_vfc(version="")
            self.update_vfc(status="")
        # Get detailed content of created VFC, especially unique_id
        urlpart = onap_utils.get_config(
            "onap.sdc.get_resource_list_url")
        url = SDC_URL2 + urlpart
        headers = SDC_HEADERS_DESIGNER
        response = requests.get(url, headers=headers,
                                proxies=PROXY, verify=False)
        if response.status_code == 200:
            result = response.json()
            for resource in result["resources"]:
                if resource["uuid"] == self.id:
                    self.__logger.info("VFC resource %s found in VFC list",
                                       resource["name"])
                    self.update_vfc(id=resource["uniqueId"],
                                    vendor_name=resource["vendorName"])
        else:
            self.__logger.error(
                "Get resource list status code : %s", response.status_code)

        self.__logger.info("VFC found parameter %s", vfc_found)
        return vfc_found

    def get_vfc_info(self):
        """
            Get vfc detail
        """
        vfc_list = self.get_vfc_list()
        for result in vfc_list:
            if result['name'] == self.name:
                self.update_vfc(version=result["version"]["id"])
                self.update_vfc(status=result["status"])
                break
        return None

    def create_vfc(self, vendor):
        """
            Create vfc in SDC (only if it does not exist)
        """
        # we check if vfc exists or not, if not we create it
        create_vfc = False
        if not self.check_vfc_exists():
            try:
                url = SDC_URL2 + onap_utils.get_config(
                    "onap.sdc.create_vfc_url")
                data = self.get_sdc_vfc_payload(action="Create",
                                                vfc_name=self.name)
                data["vendorName"] = vendor.name
                data["vendorId"] = vendor.id
                data["name"] = self.name
                data["tags"][0] = self.name
                conffilename = "onap_tests/templates/vfc/" + self.name \
                            + "_config.yml"
                if os.path.isfile(conffilename):
                    self.__logger.info(
                        "upload config yml files to VFC %s", self.name)
                    conffile = {'upload': open(conffilename, 'rb')}
                    with open(conffilename, 'rb') as conffile:
                        data["payloadData"] = base64.b64encode(conffile.read())\
                                            .decode('ascii')

                    # VFC file name to get from variable service_name
                    data["payloadName"] = self.service_name + "_config.yml"

                    data = json.dumps(data)
                    headers = SDC_UPLOAD_HEADERS_VFC
                    datachecksum = base64.b64encode(bytes(hashlib.md5(\
                        data.encode("utf-8")).hexdigest(), 'utf-8'))
                    headers["Content-MD5"] = bytes(datachecksum)
                    self.__logger.debug("Create vfc headers : %s", headers)
                    self.__logger.debug("Create vfc md5 : %s", \
                        headers["Content-MD5"])
                    self.__logger.debug("Create vfc payload : %s", data)
                    response = requests.post(url, headers=headers,
                                             proxies=PROXY, verify=True,
                                             data=data)
                    if response.status_code == 201:
                        create_vfc = True
                        vfunc = response.json()
                        self.update_vfc(unique_id=vfunc["uniqueId"],
                                        vendor_name=vfunc["vendorName"])
                        self.__logger.info(
                            "Create VFC code : %s", response.status_code)
                        self.__logger.info(
                            "VFC unique Id : %s", self.unique_id)
                    else:
                        self.__logger.error(
                            "Create VFC code : %s", response.text)
            except Exception as err:  # pylint: disable=broad-except
                self.__logger.error("Failed to create vfc: %s", err)
        else:
            self.__logger.info("vfc already exists")
        return create_vfc

    def checkin_vfc(self):
        """
            Checkin vfc
        """
        checkin_vfc = False
        old_str = onap_utils.get_config("onap.sdc.checkin_vfc_url")
        new_str = old_str.replace("PPvfc_uuidPP", self.unique_id)
        url = SDC_URL2 + new_str
        datavfc = self.get_sdc_vfc_payload(action="Checkin")
        datavfc = json.dumps(datavfc)
        headers = SDC_HEADERS_DESIGNER
        try:
            self.__logger.debug(
                "Checkin VFC url : %s", url)
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=datavfc)
            if response.status_code == 200:
                checkin_vfc = True
                self.__logger.info(
                    "Checkin VFC code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Checkin VFC code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform checkin vfc: %s", err)
        return checkin_vfc

    def new_vfc_version(self):
        """
            new_vfc_version in SDC
        """
        new_vfc_version = False
        try:
            old_str = onap_utils.get_config(
                "onap.sdc.new_vfc_version_url")
            new_str = old_str.replace("PPvfc_idPP", self.id)
            old_str = new_str
            new_str = old_str.replace("PPvfc_versionPP", self.version)
            url = SDC_URL2 + new_str
            data = self.get_sdc_vfc_payload(action="NewVersion")
            data = json.dumps(data)
            headers = SDC_HEADERS_DESIGNER
            self.__logger.debug("vfc_id : %s", self.id)
            self.__logger.debug("vfc_version : %s", self.version)
            self.__logger.debug("new_vfc_versionURL : %s", url)
            self.__logger.debug("new_vfc_version payload : %s", data)
            response = requests.post(url, headers=headers,
                                     proxies=PROXY, verify=False,
                                     data=data)
            if response.status_code == 200:
                new_vfc_version = True
                new_vfc_version_response = response.json()
                self.update_vfc(
                    status=new_vfc_version_response['status'],
                    version=new_vfc_version_response['id'])
                self.__logger.info(
                    "New version creation OK")
                self.__logger.debug(
                    "new_vfc_version version id : %s", self.version)
                self.__logger.debug(
                    "new_vfc_version status : %s", self.status)
            else:
                self.__logger.error(
                    "ERROR new_vfc_version code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to do new_vfc_version: %s", err)
        return new_vfc_version

    def upload_vfc(self, files):
        """
            upload zip for vfc
        """
        upload_vfc = False
        old_str = onap_utils.get_config("onap.sdc.upload_vfc_url")
        new_str = old_str.replace("PPvfc_idPP", self.unique_id)
        old_str = new_str
        new_str = old_str.replace("PPvfc_versionPP", self.version)
        url = SDC_URL2 + new_str
        headers = SDC_UPLOAD_HEADERS
        self.__logger.debug("upload vfc URL : %s", url)
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     files=files)
            if response.status_code == 200:
                upload_vfc = True
                self.__logger.info(
                    "Upload vfc code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Upload vfc code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform file upload\
             for the vfc: %s", err)
        return upload_vfc

    def submit_for_testing_vfc(self):
        """
            submit_for_testing vf
        """
        submit_for_testing_vfc = False
        old_str = onap_utils.get_config("onap.sdc.submit_for_testing_vfc_url")
        new_str = old_str.replace("PPvfc_uniqueidPP", self.unique_id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vfc_payload(action="Submit_for_testing")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        self.__logger.debug("Submit vfc url : %s", url)
        self.__logger.debug("Submit vfc headers : %s", headers)
        self.__logger.debug("Create vfc payload : %s", data)
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 200:
                submit_for_testing_vfc = True
                self.__logger.info(
                    "Submit VFC code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Submit VFC code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                submit_for_testing vfc: %s", err)
        return submit_for_testing_vfc

    def start_certif_vfc(self):
        """
            start_certif vfc
        """
        start_certif_vfc = False
        old_str = onap_utils.get_config("onap.sdc.start_certif_vfc_url")
        new_str = old_str.replace("PPvfc_uniqueidPP", self.unique_id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vfc_payload(action="Start_certification")
        data = json.dumps(data)
        headers = SDC_HEADERS_TESTER
        try:
            self.__logger.debug(
                "Start cerfif VFC url : %s", url)
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 200:
                start_certif_vfc = True
                self.__logger.info(
                    "start_certif VFC code : %s", response.status_code)
            else:
                self.__logger.error(
                    "start_certif VFC code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform start_certif vfc: %s", err)
        return start_certif_vfc

    def certify_vfc(self):
        """
            certify vfc
        """
        certify_vfc = False
        old_str = onap_utils.get_config("onap.sdc.certify_vfc_url")
        new_str = old_str.replace("PPvfc_uniqueidPP", self.unique_id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vfc_payload(action="Certify")
        data = json.dumps(data)
        headers = SDC_HEADERS_TESTER
        try:
            self.__logger.debug(
                "Cerfify VFC url : %s", url)
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 200:
                certify_vfc = True
                vfunc = response.json()
                self.update_vfc(id=vfunc["uniqueId"])
                self.__logger.info(
                    "Certify VFC code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Certify VFC code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform certify vfc: %s", err)
        return certify_vfc
