#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
import json
import logging
import requests
import onap_tests.utils.utils as onap_test_utils

PROXY = onap_test_utils.get_config("general.proxy")
SDNC_HEADERS = onap_test_utils.get_config("onap.sdnc.headers")
SDNC_URL = onap_test_utils.get_config("onap.sdnc.url")


class Sdnc():
    """
        SDNC main operations
    """
    __logger = logging.getLogger(__name__)

    def __init__(self):
        # Logger
        # pylint: disable=no-member
        requests.packages.urllib3.disable_warnings()

    def get_preload_list(self):
        """
            get Preload List
        """
        url = (SDNC_URL +
               "/restconf/config/VNF-API:preload-vnfs")
        self.__logger.debug("SDNC url: %s", url)
        response = requests.get(url, headers=SDNC_HEADERS,
                                proxies=PROXY, verify=False)
        self.__logger.info("SDNC response on get preload list: %s",
                           response.text)

    def get_preload_item(self, vnf_name, vnf_type):
        """
            get Preload item
        """
        url = (SDNC_URL +
               "/restconf/config/VNF-API:preload-vnfs/vnf-preload-list/" +
               vnf_name + "/" + vnf_type)
        self.__logger.debug("SDNC url: %s", url)
        response = requests.get(url, headers=SDNC_HEADERS,
                                proxies=PROXY, verify=False)
        self.__logger.info("SDNC response on get preload  %s",
                           response.text)

    @staticmethod
    def get_preload_payload(vnf_parameters, vnf_topology_identifier):
        """
            Get preload payload
        """
        # pylint: disable=anomalous-backslash-in-string
        svc_notif = "http:\/\/onap.org:8080\/adapters\/rest\/SDNCNotify"
        return json.dumps({
            "input": {
                "request-information": {
                    "notification-url": "onap.org",
                    "order-number": "1",
                    "order-version": "1",
                    "request-action": "PreloadVNFRequest",
                    "request-id": "test"
                    },
                "sdnc-request-header": {
                    "svc-action": "reserve",
                    "svc-notification-url": svc_notif,
                    "svc-request-id": "test"
                    },
                "vnf-topology-information": {
                    "vnf-assignments": {
                        "availability-zones": [],
                        "vnf-networks": [],
                        "vnf-vms": []
                    },
                    "vnf-parameters": vnf_parameters,
                    "vnf-topology-identifier": vnf_topology_identifier
                }
            }})

    def preload(self, sdnc_preload_payload):
        """
            Preload VNF
        """
        # preload_payload =
        url = (SDNC_URL +
               "/restconf/operations/VNF-API:preload-vnf-topology-operation")
        self.__logger.debug("SDNC url: %s", url)
        response = requests.post(url, headers=SDNC_HEADERS,
                                 proxies=PROXY, verify=False,
                                 data=sdnc_preload_payload)
        self.__logger.info("SDNC response on get preload %s",
                           response.status_code)
        return response.status_code

    def delete_preload(self, vnf_name, vnf_type):
        """
            Delete Preload
        """
        url = (SDNC_URL +
               "/restconf/config/VNF-API:preload-vnfs/vnf-preload-list/" +
               vnf_name + "/" + vnf_type)
        self.__logger.debug("SDNC url: %s", url)
        response = requests.delete(url, headers=SDNC_HEADERS,
                                   proxies=PROXY, verify=False)
        self.__logger.info("SDNC response on delete preload: %s",
                           response.status_code)
        return response.status_code
