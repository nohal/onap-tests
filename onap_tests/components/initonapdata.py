#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=too-many-instance-attributes
#  pylint: disable=too-many-branches
#  pylint: disable=too-many-public-methods

"""Class to define Service onboarding"""

import logging
import json
import requests
import urllib3
import onap_tests.utils.utils as onap_utils
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

PROXY = onap_utils.get_config("general.proxy")
AAI_URL = onap_utils.get_config("onap.aai.url")
VID_URL = onap_utils.get_config("onap.vid.url")
AAI_HEADERS = onap_utils.get_config("onap.aai.headers")
VID_HEADERS = onap_utils.get_config("onap.vid.headers")


class InitOnapData():
    """
        declare a generic vendor in SDC
        declare a generic customer in AAI
        declare a Cloud Region and Tenant in AAI
        declare OwningEntity, Project, Platform and LineOfBusiness in VID
    """
    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        # pylint: disable=invalid-name
        if "customer_name" in kwargs:
            self.customer_name = kwargs['customer_name']
        else:
            self.customer_name = "generic"
        self.cloud_owner = onap_utils.get_config("openstack.cloud_owner")
        self.cloud_region_id = onap_utils.get_config("openstack.region_id")
        self.tenant_id = onap_utils.get_config("openstack.tenant_id")
        self.tenant_name = onap_utils.get_config("openstack.tenant_name")

    def update_values(self, **kwargs):
        """
            Update  values
        """
        if "customer_name" in kwargs:
            self.customer_name = kwargs['customer_name']

    def get_aai_service_payload(self, **kwargs):
        """
            Build AAI service payload
        """
        try:
            payload = {}
            if kwargs['action'] == "declare_customer":
                payload = onap_utils.get_config(
                    "service_payload.declare_customer_data")
                if "customer_name" in kwargs:
                    payload['global-customer-id'] = kwargs['customer_name']
                    payload['subscriber-name'] = kwargs['customer_name']
                else:
                    payload['global-customer-id'] = "generic"
                    payload['subscriber-name'] = "generic"

            if kwargs['action'] == "declare_cloud_region":
                payload = onap_utils.get_config(
                    "service_payload.declare_cloud_region_data")
                payload['cloud-region-id'] = kwargs['cloud_region_id']

            if kwargs['action'] == "add_tenant_to_cloud":
                payload = onap_utils.get_config(
                    "service_payload.add_tenant_to_cloud_data")
                payload['tenant-id'] = kwargs['tenant_id']
                payload['tenant-name'] = kwargs['tenant_name']

        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_vid_payload(self, **kwargs):
        """
            Build VID payload
        """
        try:
            payload = {}
            if kwargs['action'] == "Declare_owningentity":
                payload = onap_utils.get_config(
                    "service_payload.declare_owningentity_data")
            if kwargs['action'] == "Declare_platform":
                payload = onap_utils.get_config(
                    "service_payload.declare_platform_data")
            if kwargs['action'] == "Declare_lineofbusiness":
                payload = onap_utils.get_config(
                    "service_payload.declare_lineofbusiness_data")
            if kwargs['action'] == "Declare_project":
                payload = onap_utils.get_config(
                    "service_payload.declare_project_data")
        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_cloud_region(self):
        """
            Get cloud region list in AAI
        """
        url = AAI_URL + onap_utils.get_config(
            "onap.aai.list_cloud_region_url")
        cloud_list = {}
        headers = AAI_HEADERS
        try:
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            cloud_list = response.json()
            self.__logger.info(
                "GET Cloud list from AAI code : %s", response.status_code)
            if response.status_code == 404:
                cloud_list = []
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform List cloud_list in AAI: %s", err)
        return cloud_list

    def get_customer_list(self):
        """
            Get customer list in AAI
        """
        url = AAI_URL + onap_utils.get_config(
            "onap.aai.list_customer_url")
        customer_list = {}
        headers = AAI_HEADERS
        try:
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            customer_list = response.json()
            self.__logger.info(
                "GET Customer list from AAI code : %s", response.status_code)
            if response.status_code == 404:
                customer_list = []
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform List customer in AAI: %s", err)
        return customer_list

    def get_tenant_list(self):
        """
            get tenant already declared with that cloud
        """
        old_str = onap_utils.get_config("onap.aai.get_tenant_url")
        new_str = old_str.replace("PPcloud_region_idPP", self.cloud_region_id)
        url = AAI_URL + new_str
        tenant_list = {}
        headers = AAI_HEADERS
        try:
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            tenant_list = response.json()
            self.__logger.info(
                "GET Tenant list from AAI code : %s", response.status_code)
            if response.status_code == 404:
                tenant_list = []
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 List tenant in AAI: %s", err)
        return tenant_list

    def check_cloud_exists(self):
        """
            Check if cloud  exists in AAI
        """
        self.__logger.info("check if %s exists", self.cloud_region_id)
        cloud_list = self.get_cloud_region()
        cloud_found_in_aai = False
        self.__logger.debug(
            "Cloud list from AAI : %s", cloud_list)
        if cloud_list:
            for result in cloud_list['cloud-region']:
                if (result['cloud-region-id'] == self.cloud_region_id and
                        result['cloud-owner'] == self.cloud_owner):
                    cloud_found_in_aai = True
        return cloud_found_in_aai

    def check_customer_exists(self):
        """
            Check if customer  exists in AAI
        """
        self.__logger.info("check if %s exists", self.customer_name)
        customer_list = self.get_customer_list()
        customer_found_in_aai = False
        self.__logger.debug(
            "Customer list from AAI : %s", customer_list)
        if customer_list:
            for result in customer_list['customer']:
                if result['global-customer-id'] == self.customer_name:
                    customer_found_in_aai = True
        return customer_found_in_aai

    def check_tenant_exists(self):
        """
            Check if tenant  exists in AAI for a cloud region
        """
        self.__logger.info("check if %s exists", self.tenant_id)
        tenant_list = self.get_tenant_list()
        tenant_found_in_aai = False
        self.__logger.debug(
            "Tenant list from AAI : %s", tenant_list)
        if tenant_list:
            for result in tenant_list['tenant']:
                if result['tenant-id'] == self.tenant_id:
                    tenant_found_in_aai = True
        return tenant_found_in_aai

    def declare_customer(self):
        """
            declare customer in AAI
        """
        declare_customer = False
        old_str = onap_utils.get_config("onap.aai.declare_customer_url")
        new_str = old_str.replace("PPcustomer_namePP", self.customer_name)
        url = AAI_URL + new_str
        data = self.get_aai_service_payload(
            action="declare_customer",
            customer_name=self.customer_name)
        data = json.dumps(data)
        headers = AAI_HEADERS
        try:
            response = requests.put(url,
                                    headers=headers,
                                    proxies=PROXY,
                                    verify=False,
                                    data=data)
            if response.status_code == 201:
                declare_customer = True
                self.__logger.info(
                    "declare_customer code : %s", response.status_code)
            else:
                self.__logger.error(
                    "declare_customer code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 declare_customer : %s", err)
        return declare_customer

    def declare_cloud_region(self):
        """
            declare Cloud Region in AAI
        """
        declare_cloud_region = False
        old_str = onap_utils.get_config(
            "onap.aai.declare_cloud_region_url")
        new_str = old_str.replace(
            "PPcloud_region_idPP", self.cloud_region_id)
        url = AAI_URL + new_str
        data = self.get_aai_service_payload(
            action="declare_cloud_region",
            cloud_region_id=self.cloud_region_id)
        data = json.dumps(data)
        headers = AAI_HEADERS
        try:
            response = requests.put(url,
                                    headers=headers,
                                    proxies=PROXY,
                                    verify=False,
                                    data=data)
            if response.status_code == 201:
                declare_cloud_region = True
                self.__logger.info(
                    "declare_cloud_region code : %s", response.status_code)
            else:
                self.__logger.error(
                    "declare_cloud_region code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform declare_cloud_region : %s", err)
        return declare_cloud_region

    def add_tenant_to_cloud(self):
        """
            Add Tenant to Cloud Region in AAI
        """
        add_tenant_to_cloud = False
        old_str = onap_utils.get_config(
            "onap.aai.add_tenant_to_cloud_url")
        new_str1 = old_str.replace("PPcloud_region_idPP", self.cloud_region_id)
        new_str2 = new_str1.replace("PPtenant_idPP", self.tenant_id)
        url = AAI_URL + new_str2
        data = self.get_aai_service_payload(
            action="add_tenant_to_cloud",
            tenant_id=self.tenant_id,
            tenant_name=self.tenant_name)
        data = json.dumps(data)
        self.__logger.info(
            "add_tenant_to_cloud url : %s", url)
        self.__logger.info(
            "add_tenant_to_cloud payload : %s", data)
        headers = AAI_HEADERS
        try:
            response = requests.put(url,
                                    headers=headers,
                                    proxies=PROXY,
                                    verify=False,
                                    data=data)
            if response.status_code == 201:
                add_tenant_to_cloud = True
                self.__logger.info(
                    "add_tenant_to_cloud code : %s", response.status_code)
            else:
                self.__logger.error(
                    "add_tenant_to_cloud code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 add_tenant_to_cloud : %s", err)
        return add_tenant_to_cloud

    def declare_owningentity(self):
        """
            declare platfom in VID
        """
        declare_owningentity = False
        new_str = onap_utils.get_config(
            "onap.vid.declare_owningentity_url")
        url = VID_URL + new_str
        data = self.get_vid_payload(
            action="Declare_owningentity")
        data = json.dumps(data)
        headers = VID_HEADERS
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 200:
                declare_owningentity = True
                self.__logger.info(
                    "declare_owningentity code : %s", response.status_code)
            else:
                self.__logger.error(
                    "declare_owningentity code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 declare_owningentity in VID : %s", err)
        return declare_owningentity

    def declare_platform(self):
        """
            declare platfom in VID
        """
        declare_platform = False
        new_str = onap_utils.get_config(
            "onap.vid.declare_platform_url")
        url = VID_URL + new_str
        data = self.get_vid_payload(action="Declare_platform")
        data = json.dumps(data)
        headers = VID_HEADERS
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 200:
                declare_platform = True
                self.__logger.info(
                    "declare_platform code : %s", response.status_code)
            else:
                self.__logger.error(
                    "declare_platform code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 declare_platform in VID : %s", err)
        return declare_platform

    def declare_line_of_business(self):
        """
            declare lineOfBusiness in VID
        """
        declare_line_of_business = False
        new_str = onap_utils.get_config(
            "onap.vid.declare_lineofbusiness_url")
        url = VID_URL + new_str
        data = self.get_vid_payload(
            action="Declare_lineofbusiness")
        data = json.dumps(data)
        headers = VID_HEADERS
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 200:
                declare_line_of_business = True
                self.__logger.info(
                    "declare_line_of_business code : %s", response.status_code)
            else:
                self.__logger.error(
                    "declare_line_of_business code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 declare_line_of_business in VID : %s", err)
        return declare_line_of_business

    def declare_project(self):
        """
            declare project in VID
        """
        declare_project = False
        new_str = onap_utils.get_config(
            "onap.vid.declare_project_url")
        url = VID_URL + new_str
        data = self.get_vid_payload(action="Declare_project")
        data = json.dumps(data)
        headers = VID_HEADERS
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 200:
                declare_project = True
                self.__logger.info(
                    "declare_project code : %s", response.status_code)
            else:
                self.__logger.error(
                    "declare_project code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                 declare_project in VID : %s", err)
        return declare_project
