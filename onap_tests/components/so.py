#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
import json
import logging
import time

import requests

import onap_tests.utils.utils as onap_test_utils
import onap_tests.utils.exceptions as onap_test_exceptions

PROXY = onap_test_utils.get_config("general.proxy")
SO_HEADERS = onap_test_utils.get_config("onap.so.headers")
SO_URL = onap_test_utils.get_config("onap.so.url")


class So():
    """
        Service orchestrator (SO) main operations
    """

    __logger = logging.getLogger(__name__)

    def __init__(self):
        # Logger
        # pylint: disable=no-member
        requests.packages.urllib3.disable_warnings()

    @classmethod
    def get_request_info(cls, instance_name):
        """
            Get Request Info
        """
        return {
            "productFamilyId": instance_name,
            "instanceName": instance_name,
            "source": "VID",
            "suppressRollback": False,
            "requestorId": "test"
        }

    @classmethod
    def get_cloud_config(cls):
        """
            Get Cloud configuration
        """
        return {
            "lcpCloudRegionId": onap_test_utils.get_config(
                "openstack.region_id"),
            "tenantId": onap_test_utils.get_config("openstack.tenant_id")
            }

    @classmethod
    def get_subscriber_info(cls):
        """
            Get Subscriber Info
        """
        subscriber_id = onap_test_utils.get_config("onap.customer")
        return {
            "globalSubscriberId": subscriber_id
        }

    @classmethod
    def get_request_param(cls, vnf, alacarte=False, subscription=False):
        """
            Get Request parameters
        """
        request_params = {
            "userParams": [],
            # "cascadeDelete": True,
            # "autoBuildVfModules": False,
            "aLaCarte": alacarte,
            "usePreload": True,
            # "rebuildVolumeGroups": False
        }
        if subscription:
            request_params.update(
                {"subscriptionServiceType": vnf})

        return request_params

    @classmethod
    def get_service_model_info(cls, invariant_uuid, uuid,
                               service_model_version="1.0"):
        """
            Return VNF model info
        """
        return {
            "modelType": "service",
            "modelName": "test-service",
            "modelInvariantId": invariant_uuid,
            "modelVersion": service_model_version,
            "modelVersionId": uuid
        }

    @classmethod
    # pylint: disable=too-many-arguments
    def get_vnf_model_info(cls, vnf_invariant_id, vnf_version_id, vnf_version,
                           vnf_model_name, vnf_customization_id,
                           vnf_customization_name):
        """
            get VNF model info
        """
        return {
            "modelType": "vnf",
            "modelInvariantId": vnf_invariant_id,
            "modelVersionId": vnf_version_id,
            "modelName": vnf_model_name,
            "modelCustomizationId": vnf_customization_id,
            "modelCustomizationName": vnf_customization_name,
            "modelVersion": vnf_version
        }

    def get_vnf_related_instance(self, instance_id, invariant_uuid, uuid,
                                 service_model_version):
        """
            Get VNF related instance
            a VNF references:
            * an instance
        """
        return {
            "instanceId": instance_id,
            "modelInfo": self.get_service_model_info(invariant_uuid,
                                                     uuid,
                                                     service_model_version)
        }

    @classmethod
    #  pylint: disable=too-many-arguments
    def get_module_model_info(cls, module_invariant_id,
                              module_name, module_customization_id,
                              module_version_id, module_version):
        """
            get Module model info
        """
        return {
            "modelType": "vfModule",
            "modelInvariantId": module_invariant_id,
            "modelCustomizationName": module_name,
            "modelName": module_name,
            "modelVersion": module_version,
            "modelCustomizationId": module_customization_id,
            "modelVersionId": module_version_id
            }

    #  pylint: disable=too-many-arguments
    def get_module_related_instance(self, vnf_id, vnf_invariant_id,
                                    vnf_version_id, vnf_version,
                                    vnf_model_name, vnf_custom_id,
                                    vnf_custom_name):
        """
            Get module related Instance.
            A module references:
            * an instance
            * a VNF
        """
        return {
            "instanceId": vnf_id,
            "modelInfo": self.get_vnf_model_info(vnf_invariant_id,
                                                 vnf_version_id,
                                                 vnf_version,
                                                 vnf_model_name,
                                                 vnf_custom_id,
                                                 vnf_custom_name)
            }
# ---------------------------------------------------------------------
# Payloads
# ---------------------------------------------------------------------

    def get_service_payload(self, vnf, request_info, model_info):
        """
            Get SO Service paylod
        """
        return json.dumps({
            "requestDetails": {
                "requestInfo": request_info,
                "modelInfo": model_info,
                "requestParameters": self.get_request_param(vnf, False, True),
                "cloudConfiguration": self.get_cloud_config(),
                "owningEntity": {
                    "owningEntityId": "OE-generic",
                    "owningEntityName": "OE-generic"},
                "platform": {
                    "platformName": "Platform-name"},
                "lineOfBusiness": {
                    "lineOfBusinessName": "Line-of-business-name"},
                "subscriberInfo": self.get_subscriber_info()
            }
        })

    def get_vnf_payload(self, vnf, request_info, vnf_model_info,
                        vnf_related_instance):
        """
            Get SO VNF payload
        """
        return json.dumps({
            "requestDetails": {
                "requestInfo": request_info,
                "modelInfo": vnf_model_info,
                "platform": {
                    "platformName": "Platform-name"},
                "requestParameters": self.get_request_param(vnf, True, True),
                "relatedInstanceList": [{
                    "relatedInstance": vnf_related_instance
                }],
                "cloudConfiguration": self.get_cloud_config(),
            }
        })

    def get_module_payload(self, vnf, request_info, module_model_info,
                           vnf_related_instance,
                           module_related_instance):
        """
            Get SO Module Instance payload
        """
        return json.dumps({
            "requestDetails": {
                "requestInfo": request_info,
                "modelInfo": module_model_info,
                "requestParameters": self.get_request_param(vnf, False, False),
                "relatedInstanceList": [
                    {"relatedInstance": vnf_related_instance},
                    {"relatedInstance": module_related_instance}],
                "cloudConfiguration": self.get_cloud_config(),
                }
            })

    def create_instance(self, so_service_payload):
        """
            SO create instance
        """
        url = SO_URL + "/ecomp/mso/infra/serviceInstances/v4"
        self.__logger.debug("SO request: %s", url)
        response = requests.post(url, headers=SO_HEADERS,
                                 proxies=PROXY, verify=False,
                                 data=so_service_payload)
        self.__logger.info("SO create service request: %s",
                           response.text)
        so_instance_id_response = response.json()
        instance_id = (
            so_instance_id_response['requestReferences']['instanceId'])
        return instance_id

    def create_vnf(self, instance_id, so_vnf_json_payload):
        """
            SO create vnf
        """
        url = (SO_URL + "/ecomp/mso/infra/serviceInstances/v4/" +
               instance_id + "/vnfs")
        self.__logger.debug("SO request: %s", url)
        response = requests.post(url, headers=SO_HEADERS,
                                 proxies=PROXY, verify=False,
                                 data=so_vnf_json_payload)
        vnf_id_response = response.json()
        self.__logger.debug("SO create VNF response %s", response.text)
        vnf_id = vnf_id_response['requestReferences']['instanceId']
        return vnf_id

    def create_module(self, instance_id, vnf_id, so_module_payload):
        """
            SO module instance
        """
        url = (SO_URL + "/ecomp/mso/infra/serviceInstances/v4/" +
               instance_id + "/vnfs/" + vnf_id + "/vfModules")
        self.__logger.info("SO create module request: %s", url)
        response = requests.post(url, headers=SO_HEADERS,
                                 proxies=PROXY, verify=False,
                                 data=so_module_payload)
        module_id = response.json()
        return module_id

    def delete_instance(self, instance_id, so_service_json_payload):
        """
            Delete SO instance
        """
        delete_instance = False
        self.__logger.info("Delete instance %s", instance_id)
        try:
            url = (SO_URL + "/ecomp/mso/infra/serviceInstances/v4/" +
                   instance_id)
            self.__logger.debug("SO request: %s", url)
            response = requests.delete(url, headers=SO_HEADERS,
                                       proxies=PROXY, verify=False,
                                       data=so_service_json_payload)
            if "202" in response.text:
                delete_instance = True
        except TypeError:
            self.__logger.error("Instance ID not defined")
        except Exception:  # pylint: disable=broad-except
            self.__logger.error("Impossible to delete the instance %s",
                                instance_id)
        return delete_instance

    def delete_vnf(self, instance_id, vnf_id, so_vnf_payload):
        """
            Delete vnf instance
        """
        delete_instance = False
        self.__logger.info("Delete vnf %s", vnf_id)
        try:
            url = (SO_URL + "/ecomp/mso/infra/serviceInstances/v4/" +
                   instance_id + "/vnfs/" + vnf_id)
            self.__logger.debug("SO request: %s", url)
            response = requests.delete(url, headers=SO_HEADERS,
                                       proxies=PROXY, verify=False,
                                       data=so_vnf_payload)
            if "202" in response.text:
                delete_instance = True
        except TypeError:
            self.__logger.error("Vnf ID not defined")
        except Exception:  # pylint: disable=broad-except
            self.__logger.error("Impossible to delete the vnf %s", vnf_id)
        return delete_instance

    def delete_module(self, so_module_payload, instance_id, vnf_id, module_id):
        """
            Delete module instance
        """
        delete_instance = False
        try:
            url = (SO_URL + "/ecomp/mso/infra/serviceInstances/v4/" +
                   instance_id + "/vnfs/" + vnf_id + "/vfModules/" + module_id)

            self.__logger.debug("SO request: %s", url)
            response = requests.delete(url, headers=SO_HEADERS,
                                       proxies=PROXY, verify=False,
                                       data=so_module_payload)
            if "202" in response.text:
                delete_instance = True
        except TypeError:
            self.__logger.error("Vnf ID not defined")
        except Exception:  # pylint: disable=broad-except
            self.__logger.error("Impossible to delete the vnf %s", vnf_id)
        return delete_instance

    def get_request_logs(self, request_id=""):
        """
            Get Info on previous request
        """
        url = (SO_URL + "/ecomp/mso/infra/orchestrationRequests/v5/" +
               request_id)
        self.__logger.debug("SO request: %s", url)
        return onap_test_utils.get_simple_request(url,
                                                  SO_HEADERS,
                                                  PROXY)

    def check_so_request_completed(self, request_id):
        """
            Check the request status in the SO
        """
        url = (SO_URL + "/ecomp/mso/infra/orchestrationRequests/v5/" +
               request_id)
        request_completed = False
        nb_try = 0
        nb_try_max = 30
        while request_completed is False and nb_try < nb_try_max:
            response = onap_test_utils.get_simple_request(url,
                                                          SO_HEADERS,
                                                          PROXY)
            self.__logger.info("SO: looking for %s request status....",
                               request_id)
            self.__logger.debug("SO answer: %s", response)
            status = response['request']['requestStatus']['requestState']
            self.__logger.info("SO: response status %s", status)
            if "complete" in status.lower():
                self.__logger.info("Request completed")
                request_completed = True
            if "failed" in status.lower():
                raise onap_test_exceptions.SoCompletionException
            time.sleep(10)
            nb_try += 1

        if request_completed is False:
            self.__logger.info("Request not seen completed")
            raise onap_test_exceptions.SoRequestException

        return request_completed
