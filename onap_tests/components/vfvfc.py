#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
import logging
import json
import datetime
import base64
import hashlib
import yaml
import requests
import onap_tests.utils.utils as onap_utils

PROXY = onap_utils.get_config("general.proxy")
SDC_URL = onap_utils.get_config("onap.sdc.url")
SDC_URL2 = onap_utils.get_config("onap.sdc.url2")
SDC_HEADERS = onap_utils.get_config("onap.sdc.headers")
SDC_HEADERS_DESIGNER = SDC_HEADERS.copy()
SDC_HEADERS_DESIGNER["USER_ID"] = "cs0008"
SDC_HEADERS_TESTER = SDC_HEADERS.copy()
SDC_HEADERS_TESTER["USER_ID"] = "jm0007"
SDC_UPLOAD_HEADERS = onap_utils.get_config("onap.sdc.upload_headers")
SDC_UPLOAD_HEADERS_VFC = onap_utils.get_config("onap.sdc.vfc_upload_headers")


class Vfvfc():
    """
        ONAP VF_fromvfc Object
        Used for SDC operations
    """
    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        # pylint: disable=invalid-name
        # init for Vfvfc
        self.id = ""
        self.unique_id = ""
        if "vfc_vf_name" in kwargs:
            self.name = kwargs['vfc_vf_name']
        else:
            self.name = ""
        self.version = ""
        self.status = ""
        self.posx = 200
        self.posy = 200

    def update_vf(self, **kwargs):
        """
            Update VF values
        """
        # update for Vfvfc
        if "id" in kwargs:
            self.id = kwargs['id']
        if "unique_id" in kwargs:
            self.unique_id = kwargs['unique_id']
        if "name" in kwargs:
            self.name = kwargs['name']
        # VFvfc init
        if "version" in kwargs:
            self.version = kwargs['version']
        if "status" in kwargs:
            self.status = kwargs['status']

    def get_sdc_vf_payload(self, **kwargs):
        """
            Build SDC VF payload
        """
        # Get config from onap_testing.yaml
        try:
            payload = {}
            if kwargs['action'] == "Create":
                payload = onap_utils.get_config(
                    "vf_payload.vf_vfc_create_data")
                if "vendor_name" and "vfc_name" and \
                   "vf_name" in kwargs:
                    payload['vendorName'] = kwargs['vendor_name']
                    payload['tags'][0] = kwargs['vf_name']
                    payload['name'] = kwargs['vf_name']
            if kwargs['action'] == "addVFC":
                payload = onap_utils.get_config(
                    "vf_payload.vf_addVFC")
                if "vfc_name" and "vfc_id" in kwargs:
                    payload['componentUid'] = kwargs['vfc_id']
                    payload['name'] = kwargs['vfc_name']
                    dtime = datetime.datetime.now()
                    payload['uniqueId'] = kwargs['vfc_id'] + \
                                          dtime.strftime("%s")
            if kwargs['action'] == "declare_properties_vfvfc":
                payload = onap_utils.get_config(
                    "vf_payload.vf_attributes")
                self.__logger.debug("payload value initial: %s", payload)
                payloadstring = str(payload)
                payloadstring = payloadstring.replace(\
                    "PPvf_uniqueidPP", kwargs['vf_uniqueid'])
                payloadstring = payloadstring.replace(\
                    "PPvfc_CertidPP", kwargs['vfc_certid'])
                payloadstring = payloadstring.replace("PPvfc_uniqueidPP",\
                    kwargs['vfc_uniqueid'])
                payloadstring = payloadstring.replace("PPvfcnamePP",\
                    kwargs['vfc_name'])
                payloadstring = payloadstring.replace("PPvfnamePP",\
                    kwargs['vf_name'])
                payloadstring = payloadstring.replace("None", "null")
                self.__logger.debug("payload value inter: %s", payloadstring)
                payload = yaml.load(payloadstring)
                self.__logger.debug("payload value final: %s", payload)
            if kwargs['action'] == "vf_getInputs":
                payload = onap_utils.get_config(
                    "vf_payload.vf_getInputs")
            if kwargs['action'] == "VFCheckin":
                payload = onap_utils.get_config(
                    "vf_payload.vf_checkin_data")
            if kwargs['action'] == "VFSubmit_for_testing":
                payload = onap_utils.get_config(
                    "vf_payload.vf_submit_for_testing_data")
            if kwargs['action'] == "VFStart_certification":
                payload = onap_utils.get_config(
                    "vf_payload.vf_start_certification_data")
            if kwargs['action'] == "VFCertify":
                payload = onap_utils.get_config(
                    "vf_payload.vf_certify_data")
        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_vfvfc_list(self):
        """
            Get vf list
        """
        url = SDC_URL + onap_utils.get_config(
            "onap.sdc.list_vf_url")
        vf_list = {}
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            vf_list = response.json()
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("vfvfc - Failed to get vf list: %s", err)
        return vf_list

    def get_vf_metadata(self):
        """
            Get vfvfc metadata to get vf unique_id when version is changing
        """
        vfvfc_unique_id_updated = False
        old_str = onap_utils.get_config("onap.sdc.metadata_vf_url")
        self.__logger.debug("Test valueVF unique Id : %s", self.unique_id)
        if self.unique_id:
            new_str = old_str.replace("PPvf_unique_idPP", self.unique_id)
            url = SDC_URL2 + new_str
            headers = SDC_HEADERS_DESIGNER
            try:
                self.__logger.debug(
                    "Get VFvfc metadata code - url : %s", url)
                response = requests.get(url, headers=headers,
                                        proxies=PROXY, verify=False)
                if response.status_code == 200:
                    vfvfc_unique_id_updated = True
                    vf_meta = response.json()
                    # only working with 1.0 version
                    self.update_vf(
                        unique_id=vf_meta["metadata"]["allVersions"]["1.0"])
                    self.__logger.debug(
                        "Get VFvfc metadata code : %s", response)
                    self.__logger.info(
                        "Get VFvfc metadata code : %s", response.status_code)
                    self.__logger.info("VF unique Id : %s", self.unique_id)
                else:
                    self.__logger.error(
                        "Get VFvfc metadata code : %s", response.status_code)
                    self.__logger.error("VFvfc unique Id : %s", self.unique_id)
            except Exception as err:  # pylint: disable=broad-except
                self.__logger.error("Failed to get VFvfc metadata: %s", err)
        return vfvfc_unique_id_updated

    def check_vf_exists(self):
        """
            Check if provided vf exists in vf list
        """
        vf_list = self.get_vfvfc_list()
        vf_found = False
        for result in vf_list:
            if result['name'] == self.name:
                vf_found = True
                self.__logger.info("VFvfc %s found in VF list", self.name)
                self.update_vf(id=result["uuid"])
                self.update_vf(version=result["version"])
                self.update_vf(status=result["lifecycleState"])
        if not vf_found:
            self.__logger.info("VFvfc in not in vf list: %s", self.name)
            self.update_vf(id="")
            self.update_vf(unique_id="")
            self.update_vf(version="")
            self.update_vf(status="")
        if vf_found:
            # Get detailed content of created VFvfc, especially unique_id
            urlpart = onap_utils.get_config(
                "onap.sdc.get_resource_list_url")
            url = SDC_URL2 + urlpart
            headers = SDC_HEADERS_DESIGNER
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            if response.status_code == 200:
                answer = response.json()
                for resource in answer["resources"]:
                    if resource["uuid"] == self.id:
                        self.__logger.info(
                            "VFvfc resource %s found in resource list",
                            resource["name"])
                        self.update_vf(unique_id=resource["uniqueId"],
                                       vendor_name=resource["vendorName"])
            else:
                self.__logger.error(
                    "Get VFvfc resource list status code : %s", \
                    response.status_code)

            self.__logger.info("VFvfc found parameter %s", vf_found)

        return vf_found

    def create_vfvfc(self, vfc):
        """
            Create vf in SDC (only if it does not exist)
        """
        # we check if vf exists or not, if not we create it
        create_vfvfc = False
        if not self.check_vf_exists():
            try:
                url = SDC_URL2 + onap_utils.get_config(
                    "onap.sdc.create_vf_url")
                data = self.get_sdc_vf_payload(action="Create",
                                               vf_name=self.name,
                                               vendor_name=vfc.vendor_name,
                                               vfc_name=vfc.name)
                data = json.dumps(data)
                headers = SDC_UPLOAD_HEADERS_VFC
                datachecksum = base64.b64encode(bytes(hashlib.md5(\
                    data.encode("utf-8")).hexdigest(), 'utf-8'))
                headers["Content-MD5"] = bytes(datachecksum)
                self.__logger.debug("Create vfvfc headers : %s", headers)
                self.__logger.debug("Create vfvfc md5 : %s",
                                    headers["Content-MD5"])
                self.__logger.debug("Create vfvfc payload : %s", data)
                self.__logger.debug(
                    "Create VFvfc url : %s", url)
                self.__logger.debug(
                    "Create VFvfc headers : %s", headers)
                self.__logger.debug(
                    "Create VFvfc data : %s", data)
                response = requests.post(url, headers=headers,
                                         proxies=PROXY, verify=False,
                                         data=data)
                if response.status_code == 201:
                    create_vfvfc = True
                    vfunc = response.json()
                    self.update_vf(id=vfunc["uuid"])
                    self.update_vf(unique_id=vfunc["uniqueId"])
                    self.__logger.info(
                        "Create VFvfc code : %s", response.status_code)
                    self.__logger.info(
                        "VFvfc Id : %s", self.id)
                    self.__logger.info(
                        "VFvfc unique Id : %s", self.unique_id)
                else:
                    self.__logger.debug(
                        "Create VFvfc response : %s", response)
                    self.__logger.error(
                        "Create VFvfc code : %s", response.status_code)
            except Exception as err:  # pylint: disable=broad-except
                self.__logger.error("Failed to create vf: %s", err)
        return create_vfvfc

    def addvfcvf(self, vfc):
        """
            Add VFC to vf in SDC (only if it does not exist)
        """

        try:
            old_str = onap_utils.get_config("onap.sdc.addVFC_vf_url")
            new_str = old_str.replace("PPvf_uniqueidPP", self.unique_id)
            url = SDC_URL2 + new_str
            data = self.get_sdc_vf_payload(action="addVFC",
                                           vfc_name=vfc.name,
                                           vfc_id=vfc.id)
            data = json.dumps(data)
            headers = SDC_HEADERS_DESIGNER
            self.__logger.debug("Add VFC to VF url : %s", url)
            self.__logger.debug("Add VFC to VF headers : %s", headers)
            self.__logger.debug("Add VFC to VF data : %s", data)
            response = requests.post(url, headers=headers,
                                     proxies=PROXY, verify=False,
                                     data=data)
            if response.status_code == 201:
                self.__logger.info("AddVFC to VF code : %s",
                                   response.status_code)
            else:
                self.__logger.error(
                    "AddVFC to VF code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to create vf: %s", err)

    def declarepropertiesvf(self, vfc):
        """
        Declare properties assignments for vf in SDC
        """
        try:
            old_str = onap_utils.get_config(
                "onap.sdc.declare_properties_vf_url")
            new_str = old_str.replace("PPvf_uniqueidPP", self.unique_id)
            url = SDC_URL2 + new_str
            data = self.get_sdc_vf_payload(action="declare_properties_vfvfc",
                                           vf_name=self.name,
                                           vfc_name=vfc.name.lower(),
                                           vf_uniqueid=self.unique_id,
                                           vfc_certid=vfc.id,
                                           vfc_uniqueid=vfc.unique_id)
            data = json.dumps(data)
            headers = SDC_HEADERS_DESIGNER
            self.__logger.debug(
                "Declare properties assignments for vf url: %s", url)
            self.__logger.debug(
                "Declare properties assignments for vf headers: %s", headers)
            self.__logger.debug(
                "Declare properties assignments for vf data: %s", data)
            response = requests.post(url, headers=headers,
                                     proxies=PROXY, verify=False,
                                     data=data)
            if response.status_code == 200:
                self.__logger.info(
                    "Declare properties assignments for vf : %s",\
                    response.status_code)
            else:
                self.__logger.error(
                    "Declare properties assignments for vf : %s",\
                     response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Declare properties assignments for vf %s",\
                 err)

    def extractsubuniqueid(self, response, inputs):
        # Goal is to replace PPownerIdPP in the inputs list using the name
        # as searchkey

        output = []
        for entry in inputs:
            for resp in response:
                if  entry["name"] == resp["name"]:
                    ownerid = resp["ownerId"]
                    self.__logger.debug(
                        "Substitute identification ownerid: %s", ownerid)
                    resp["defaultValue"] = entry["defaultValue"]
                    output.append(resp)
        self.__logger.debug(
            "Substitute identification output: %s", output)
        return output

    def modifyinputs(self):
        try:
            payload = onap_utils.get_config("vf_payload.vf_write_inputs")
            self.__logger.debug(
                "Modify inputs for vf payload: %s", payload)
            if payload:
                #Perform get requests to get specific ownerId and overwrite
                #inputs to set for VF
                old_str = onap_utils.get_config("onap.sdc.getInputs_vf_url")
                new_str = old_str.replace("PPvf_uniqueidPP", self.unique_id)
                url = SDC_URL2 + new_str
                # data = self.get_sdc_vf_payload(action="vf_getInputs")
                # data = json.dumps(data)
                headers = SDC_HEADERS_DESIGNER
                self.__logger.debug("Get inputs url : %s", url)
                self.__logger.debug("Get inputs headers : %s", headers)
                #self.__logger.debug("Get inputs data : %s", data)
                response = requests.get(url, headers=headers,
                                        proxies=PROXY, verify=False)
                if response.status_code == 200:
                    result = response.json()
                    self.__logger.debug("Get inputs response : %s", result)
                    inputstoset = self.extractsubuniqueid(result["inputs"],
                                                          payload)
                    self.__logger.info(
                        "Assignements inputs to modify for vf : %s",\
                        inputstoset)
                else:
                    self.__logger.error(
                        "Error while getting inputs for vf : %s",\
                         response.status_code)
                #Set modified inputs for VF
                old_str = onap_utils.get_config(\
                "onap.sdc.modifyInputs_vf_url")
                new_str = old_str.replace("PPvf_uniqueidPP", self.unique_id)
                url = SDC_URL2 + new_str
                data = json.dumps(inputstoset)
                headers = SDC_HEADERS_DESIGNER
                self.__logger.debug("Modify inputs url : %s", url)
                self.__logger.debug("Modify inputs headers : %s", headers)
                self.__logger.debug("Modify inputs data : %s", data)
                response = requests.post(url, headers=headers,
                                         proxies=PROXY, verify=False,
                                         data=data)
                if response.status_code == 200:
                    self.__logger.info(
                        "Assignements inputs modified for vf : %s",\
                         inputstoset)
                else:
                    self.__logger.error(
                        "Error while modifying inputs for vf : %s", \
                        response.status_code)
            else:
                self.__logger.info("No inputs to modify for vf")
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Modify inputs for vf %s", err)

    def upgrade_vfvfc(self, vfc):
        """
            upgrade vf in SDC
        """
        upgrade_vfvfc = False
        try:
            url = SDC_URL2 + onap_utils.get_config(
                "onap.sdc.upgrade_vf_url")
            data = self.get_sdc_vf_payload(action="Upgrade",
                                           vf_name=self.name,
                                           vendor_name=vfc.vendor_name,
                                           vfc_name=vfc.name,
                                           csar_uuid=vfc.csar_uuid)
            data = json.dumps(data)
            headers = SDC_HEADERS_DESIGNER
            response = requests.post(url, headers=headers,
                                     proxies=PROXY, verify=False,
                                     data=data)
            if response.status_code == 201:
                upgrade_vfvfc = True
                vfunc = response.json()
                self.update_vf(id=vfunc["uuid"])
                self.update_vf(unique_id=vfunc["uniqueId"])
                self.__logger.info(
                    "Upgrade VFvfc code : %s", response.status_code)
                self.__logger.info(
                    "VFvfc Id : %s", self.id)
                self.__logger.info(
                    "VFvfc unique Id : %s", self.unique_id)
            else:
                self.__logger.error(
                    "Upgrade VFvfc code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to upgrade VFvfc: %s", err)
        return upgrade_vfvfc

    def get_vf_info(self, vfunction):
        """
            Get vf detail
        """
        vf_list = self.get_vfvfc_list()
        for result in vf_list:
            if result['name'] == vfunction['name']:
                vfunction = result
                break
        return vfunction

    def checkin_vfvfc(self):
        """
            Checkin vf
        """
        checkin_vfvfc = False
        old_str = onap_utils.get_config("onap.sdc.checkin_vf_url")
        new_str = old_str.replace("PPvf_uuidPP", self.id)
        url = SDC_URL + new_str
        datavf = self.get_sdc_vf_payload(action="VFCheckin")
        datavf = json.dumps(datavf)
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=datavf)
            if response.status_code == 201:
                checkin_vfvfc = True
                self.__logger.info(
                    "Checkin VF code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Checkin VF code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform checkin vf: %s", err)
        return checkin_vfvfc

    def checkout_vfvfc(self):
        """
            Checkout vf
        """
        checkout_vfvfc = False
        old_str = onap_utils.get_config("onap.sdc.checkout_vf_url")
        new_str = old_str.replace("PPvf_uuidPP", self.unique_id)
        url = SDC_URL2 + new_str
        headers = SDC_HEADERS_DESIGNER
        try:
            self.__logger.debug("Checkout url : %s", url)
            self.__logger.debug("Checkout headers : %s", headers)
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False)
            if response.status_code == 200:
                checkout_vfvfc = True
                vf_meta = response.json()
                # only working with 1.0 version
                self.update_vf(unique_id=vf_meta["uniqueId"])
                self.__logger.info(
                    "Checkout VFvfc code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Checkout VFvfc code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform checkout VFvfc: %s", err)
        return checkout_vfvfc

    def submit_for_testing_vfvfc(self):
        """
            submit_for_testing vf
        """
        submit_for_testing_vfvfc = False
        old_str = onap_utils.get_config("onap.sdc.submit_for_testing_vf_url")
        new_str = old_str.replace("PPvf_uuidPP", self.id)
        url = SDC_URL + new_str
        datavf = self.get_sdc_vf_payload(action="VFSubmit_for_testing")
        datavf = json.dumps(datavf)
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=datavf)
            if response.status_code == 201:
                submit_for_testing_vfvfc = True
                self.__logger.info(
                    "Submit VF code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Submit VF code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                submit_for_testing vf: %s", err)
        return submit_for_testing_vfvfc

    def start_certif_vfvfc(self):
        """
            start_certif vf
        """
        start_certif_vfvfc = False
        old_str = onap_utils.get_config("onap.sdc.start_certif_vf_url")
        new_str = old_str.replace("PPvf_uuidPP", self.id)
        url = SDC_URL + new_str
        datavf = self.get_sdc_vf_payload(action="VFStart_certification")
        datavf = json.dumps(datavf)
        headers = SDC_HEADERS_TESTER
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=datavf)
            if response.status_code == 201:
                start_certif_vfvfc = True
                self.__logger.info(
                    "start_certif VFvfc code : %s", response.status_code)
            else:
                self.__logger.error(
                    "start_certif VFvfc code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform start_certif VFvfc: %s", err)
        return start_certif_vfvfc

    def certify_vfvfc(self):
        """
            certify vfvfc
        """
        certify_vfvfc = False
        old_str = onap_utils.get_config("onap.sdc.certify_vf_url")
        updated_str = old_str.replace("PPvf_uuidPP", self.id)
        url = SDC_URL + updated_str
        datavf = self.get_sdc_vf_payload(action="VFCertify")
        datavf = json.dumps(datavf)
        headers = SDC_HEADERS_TESTER
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=datavf)
            if response.status_code == 201:
                certify_vfvfc = True
                self.__logger.info(
                    "Certify VFvfc code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Certify VFvfc code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform certify VFvfc: %s", err)
        return certify_vfvfc
