#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
import logging
import json
import requests
import onap_tests.utils.utils as onap_utils

PROXY = onap_utils.get_config("general.proxy")
SDC_URL = onap_utils.get_config("onap.sdc.url")
SDC_URL2 = onap_utils.get_config("onap.sdc.url2")
SDC_HEADERS = onap_utils.get_config("onap.sdc.headers")
SDC_HEADERS_DESIGNER = SDC_HEADERS.copy()
SDC_HEADERS_DESIGNER["USER_ID"] = "cs0008"
SDC_HEADERS_TESTER = SDC_HEADERS.copy()
SDC_HEADERS_TESTER["USER_ID"] = "jm0007"
SDC_UPLOAD_HEADERS = onap_utils.get_config("onap.sdc.upload_headers")


class VF():
    """
        ONAP VF Object
        Used for SDC operations
    """
    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        # pylint: disable=invalid-name
        self.id = ""
        self.unique_id = ""
        if "vf_name" in kwargs:
            self.name = kwargs['vf_name']
        else:
            self.name = "ONAP-test-VF"
        self.version = ""
        self.status = ""
        self.posx = 200
        self.posy = 200

    def update_vf(self, **kwargs):
        """
            Update VF values
        """
        if "id" in kwargs:
            self.id = kwargs['id']
        if "unique_id" in kwargs:
            self.unique_id = kwargs['unique_id']
        if "name" in kwargs:
            self.name = kwargs['name']
        if "version" in kwargs:
            self.version = kwargs['version']
        if "status" in kwargs:
            self.status = kwargs['status']

    def get_sdc_vf_payload(self, **kwargs):
        """
            Build SDC VF payload
        """
        try:
            payload = {}
            if kwargs['action'] == "Create":
                payload = onap_utils.get_config(
                    "vf_payload.vf_create_data")
                if "vendor_name" and "vsp_name" and \
                   "vf_name" and "csar_uuid" in kwargs:
                    payload['vendorName'] = kwargs['vendor_name']
                    payload['tags'][0] = kwargs['vf_name']
                    payload['name'] = kwargs['vf_name']
                    payload['csarUUID'] = kwargs['csar_uuid']
            if kwargs['action'] == "Checkin":
                payload = onap_utils.get_config(
                    "vf_payload.vf_checkin_data")
            if kwargs['action'] == "Submit_for_testing":
                payload = onap_utils.get_config(
                    "vf_payload.vf_submit_for_testing_data")
            if kwargs['action'] == "Start_certification":
                payload = onap_utils.get_config(
                    "vf_payload.vf_start_certification_data")
            if kwargs['action'] == "Certify":
                payload = onap_utils.get_config(
                    "vf_payload.vf_certify_data")
        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_vf_list(self):
        """
            Get vf list
        """
        url = SDC_URL + onap_utils.get_config(
            "onap.sdc.list_vf_url")
        vf_list = {}
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            vf_list = response.json()
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to get vf list: %s", err)
        return vf_list

    def get_vf_metadata(self):
        """
            Get vf metadata to get vf unique_id when version is changing
        """
        vf_unique_id_updated = False
        old_str = onap_utils.get_config("onap.sdc.metadata_vf_url")
        new_str = old_str.replace("PPvf_unique_idPP", self.unique_id)
        url = SDC_URL2 + new_str
        headers = SDC_HEADERS_DESIGNER
        try:
            self.__logger.debug(
                "Get VF metadata url : %s", url)
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            if response.status_code == 200:
                vf_unique_id_updated = True
                vf_meta = response.json()
                # only working with 1.0 version
                self.update_vf(
                    unique_id=vf_meta["metadata"]["allVersions"]["1.0"])
                self.__logger.info(
                    "Get VF metadata code : %s", response.status_code)
                self.__logger.info(
                    "VF unique Id : %s", self.unique_id)
            else:
                self.__logger.error(
                    "Get VF metadata code : %s", response.status_code)
                self.__logger.error(
                    "VF unique Id : %s", self.unique_id)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to get vf metadata: %s", err)
        return vf_unique_id_updated

    def check_vf_exists(self):
        """
            Check if provided vf exists in vf list
        """
        vf_list = self.get_vf_list()
        vf_found = False
        for result in vf_list:
            if result['name'] == self.name:
                vf_found = True
                self.__logger.info("VF %s found in VF list", self.name)
                self.update_vf(id=result["uuid"])
                self.update_vf(version=result["version"])
                self.update_vf(status=result["lifecycleState"])
        if not vf_found:
            self.__logger.info("VF in not in vf list: %s", self.name)
            self.update_vf(id="")
            self.update_vf(unique_id="")
            self.update_vf(version="")
            self.update_vf(status="")

        if vf_found:
            # Get detailed content of created VF, especially unique_id
            urlpart = onap_utils.get_config(
                "onap.sdc.get_resource_list_url")
            url = SDC_URL2 + urlpart
            headers = SDC_HEADERS_DESIGNER
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            if response.status_code == 200:
                result = response.json()
                for resource in result["resources"]:
                    if resource["uuid"] == self.id:
                        self.__logger.info(
                            "VF resource %s found in resource list",
                            resource["name"])
                        self.__logger.debug(
                            "VF resource %s uniqueId",
                            resource["uniqueId"])
                        self.update_vf(unique_id=resource["uniqueId"],
                                       vendor_name=resource["vendorName"])
            else:
                self.__logger.error(
                    "Get VF resource list status code : %s", \
                    response.status_code)

            self.__logger.info("VF found parameter %s", vf_found)

        return vf_found

    def create_vf(self, vsp):
        """
            Create vf in SDC (only if it does not exist)
        """
        # we check if vf exists or not, if not we create it
        create_vf = False
        if not self.check_vf_exists():
            try:
                url = SDC_URL2 + onap_utils.get_config(
                    "onap.sdc.create_vf_url")
                data = self.get_sdc_vf_payload(action="Create",
                                               vf_name=self.name,
                                               vendor_name=vsp.vendor_name,
                                               vsp_name=vsp.name,
                                               csar_uuid=vsp.csar_uuid)
                data = json.dumps(data)
                headers = SDC_HEADERS_DESIGNER
                response = requests.post(url, headers=headers,
                                         proxies=PROXY, verify=False,
                                         data=data)
                if response.status_code == 201:
                    create_vf = True
                    vfunc = response.json()
                    self.update_vf(id=vfunc["uuid"])
                    self.update_vf(unique_id=vfunc["uniqueId"])
                    self.__logger.info(
                        "Create VF code : %s", response.status_code)
                    self.__logger.info(
                        "VF Id : %s", self.id)
                    self.__logger.info(
                        "VF unique Id : %s", self.unique_id)
                else:
                    self.__logger.error(
                        "Create VF code : %s", response.status_code)
            except Exception as err:  # pylint: disable=broad-except
                self.__logger.error("Failed to create vf: %s", err)
        return create_vf

    def upgrade_vf(self, vsp):
        """
            upgrade vf in SDC
        """
        upgrade_vf = False
        try:
            url = SDC_URL2 + onap_utils.get_config(
                "onap.sdc.upgrade_vf_url")
            data = self.get_sdc_vf_payload(action="Upgrade",
                                           vf_name=self.name,
                                           vendor_name=vsp.vendor_name,
                                           vsp_name=vsp.name,
                                           csar_uuid=vsp.csar_uuid)
            data = json.dumps(data)
            headers = SDC_HEADERS_DESIGNER
            response = requests.post(url, headers=headers,
                                     proxies=PROXY, verify=False,
                                     data=data)
            if response.status_code == 201:
                upgrade_vf = True
                vfunc = response.json()
                self.update_vf(id=vfunc["uuid"])
                self.update_vf(unique_id=vfunc["uniqueId"])
                self.__logger.info(
                    "Upgrade VF code : %s", response.status_code)
                self.__logger.info(
                    "VF Id : %s", self.id)
                self.__logger.info(
                    "VF unique Id : %s", self.unique_id)
            else:
                self.__logger.error(
                    "Upgrade VF code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to upgrade vf: %s", err)
        return upgrade_vf

    def get_vf_info(self, vfunction):
        """
            Get vf detail
        """
        vf_list = self.get_vf_list()
        for result in vf_list:
            if result['name'] == vfunction['name']:
                vfunction = result
                break
        return vfunction

    def checkin_vf(self):
        """
            Checkin vf
        """
        checkin_vf = False
        old_str = onap_utils.get_config("onap.sdc.checkin_vf_url")
        new_str = old_str.replace("PPvf_uuidPP", self.id)
        url = SDC_URL + new_str
        data = self.get_sdc_vf_payload(action="Checkin")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 201:
                checkin_vf = True
                self.__logger.info(
                    "Checkin VF code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Checkin VF code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform checkin vf: %s", err)
        return checkin_vf

    def checkout_vf(self):
        """
            Checkout vf
        """
        checkout_vf = False
        old_str = onap_utils.get_config("onap.sdc.checkout_vf_url")
        new_str = old_str.replace("PPvf_uuidPP", self.id)
        url = SDC_URL + new_str
        data = self.get_sdc_vf_payload(action="Checkout")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 201:
                checkout_vf = True
                self.__logger.info(
                    "Checkout VF code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Checkout VF code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform checkout vf: %s", err)
        return checkout_vf

    def submit_for_testing_vf(self):
        """
            submit_for_testing vf
        """
        submit_for_testing_vf = False
        old_str = onap_utils.get_config("onap.sdc.submit_for_testing_vf_url")
        new_str = old_str.replace("PPvf_uuidPP", self.id)
        url = SDC_URL + new_str
        data = self.get_sdc_vf_payload(action="Submit_for_testing")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 201:
                submit_for_testing_vf = True
                self.__logger.info(
                    "Submit VF code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Submit VF code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform\
                submit_for_testing vf: %s", err)
        return submit_for_testing_vf

    def start_certif_vf(self):
        """
            start_certif vf
        """
        start_certif_vf = False
        old_str = onap_utils.get_config("onap.sdc.start_certif_vf_url")
        new_str = old_str.replace("PPvf_uuidPP", self.id)
        url = SDC_URL + new_str
        data = self.get_sdc_vf_payload(action="Start_certification")
        data = json.dumps(data)
        headers = SDC_HEADERS_TESTER
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 201:
                start_certif_vf = True
                self.__logger.info(
                    "start_certif VF code : %s", response.status_code)
            else:
                self.__logger.error(
                    "start_certif VF code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform start_certif vf: %s", err)
        return start_certif_vf

    def certify_vf(self):
        """
            certify vf
        """
        certify_vf = False
        old_str = onap_utils.get_config("onap.sdc.certify_vf_url")
        new_str = old_str.replace("PPvf_uuidPP", self.id)
        url = SDC_URL + new_str
        data = self.get_sdc_vf_payload(action="Certify")
        data = json.dumps(data)
        headers = SDC_HEADERS_TESTER
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 201:
                certify_vf = True
                self.__logger.info(
                    "Certify VF code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Certify VF code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform certify vf: %s", err)
        return certify_vf
