#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
import logging
import json
from copy import deepcopy
import requests
import onap_tests.utils.exceptions as onap_test_exceptions
import onap_tests.utils.utils as onap_utils

PROXY = onap_utils.get_config("general.proxy")
SDC_URL = onap_utils.get_config("onap.sdc.url")
SDC_URL2 = onap_utils.get_config("onap.sdc.url2")
SDC_HEADERS = onap_utils.get_config("onap.sdc.headers")
SDC_HEADERS_DESIGNER = SDC_HEADERS.copy()
SDC_HEADERS_DESIGNER["USER_ID"] = "cs0008"
SDC_UPLOAD_HEADERS = onap_utils.get_config("onap.sdc.upload_headers")


class VSP():
    """
        ONAP VSP Object
        Used for SDC operations
    """
    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        # pylint: disable=invalid-name
        self.id = ""
        if "vsp_name" in kwargs:
            self.name = kwargs['vsp_name']
        else:
            self.name = "ONAP-test-VSP"
        self.version = ""
        self.status = ""
        self.csar_uuid = ""
        self.vendor_name = ""

    def update_vsp(self, **kwargs):
        """
            Update vendor values
        """
        if "id" in kwargs:
            self.id = kwargs['id']
        if "name" in kwargs:
            self.name = kwargs['name']
        if "version" in kwargs:
            self.version = kwargs['version']
        if "status" in kwargs:
            self.status = kwargs['status']
        if "csar_uuid" in kwargs:
            self.csar_uuid = kwargs['csar_uuid']
        if "vendor_name" in kwargs:
            self.vendor_name = kwargs['vendor_name']

    def get_sdc_vsp_payload(self, **kwargs):
        """
            Build SDC VSP payload
        """
        try:
            payload = {}
            if kwargs['action'] == "Create":
                payload = onap_utils.get_config(
                    "vsp_payload.vsp_create_data")
                if "vsp_name" in kwargs:
                    payload['name'] = kwargs['vsp_name']
                else:
                    payload['name'] = "ONAP-test-VSP"
            if kwargs['action'] == "Commit":
                payload = onap_utils.get_config(
                    "vsp_payload.vsp_commit_data")
            if kwargs['action'] == "Validate":
                payload = onap_utils.get_config(
                    "vsp_payload.vsp_validate_data")
            if kwargs['action'] == "Create_Package":
                payload = onap_utils.get_config(
                    "vsp_payload.vsp_create_package_data")
            if kwargs['action'] == "Submit":
                payload = onap_utils.get_config(
                    "vsp_payload.vsp_submit_data")
            if kwargs['action'] == "NewVersion":
                payload = onap_utils.get_config(
                    "vsp_payload.new_vsp_version_data")
        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_vsp_list(self):
        """
            Get vsp list
        """
        url = SDC_URL2 + onap_utils.get_config(
            "onap.sdc.list_vsp_url")
        vsp_list = {}
        try:
            headers = SDC_HEADERS_DESIGNER
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            vsp_list = response.json()
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to get vsp list: %s", err)
            raise onap_test_exceptions.VspNotFoundException
        return vsp_list

    def get_last_version(self, vsp_versions):
        highest_version = {}
        list_version = []
        for elem in vsp_versions['results']:
            list_version.append(int(elem['name'][:-2]))
        max_version = max(list_version)
        for elem in vsp_versions['results']:
            if int(elem['name'][:-2]) == int(max_version):
                highest_version = deepcopy(elem)
        self.update_vsp(status=highest_version['status'])
        self.update_vsp(version=highest_version['id'])
        self.__logger.debug("VERSION : %s", self.version)

    def check_vsp_exists(self):
        """
            Check if provided vsp exists in vsp list
        """
        vsp_list = self.get_vsp_list()
        vsp_found = False
        for result in vsp_list['results']:
            if result['name'] == self.name:
                vsp_found = True
                self.__logger.debug(
                    "VSP data : %s",
                    result)
                self.update_vsp(id=result['id'])
                old_str = onap_utils.get_config(
                    "onap.sdc.get_vsp_status_url")
                new_str = old_str.replace("PPvsp_idPP", self.id)
                url = SDC_URL2 + new_str
                headers = SDC_HEADERS_DESIGNER
                response = requests.get(url, headers=headers,
                                        proxies=PROXY, verify=False)
                if response.status_code == 200:
                    check_vsp_response = response.json()
                    self.__logger.debug(
                        "check_vsp_exists response : %s",
                        check_vsp_response)
                    self.get_last_version(check_vsp_response)
                    self.update_vsp(vendor_name=result['vendorName'])
                    self.__logger.info(
                        "Get vsp info from SDC OK")
                else:
                    self.__logger.error(
                        "Get vsp status code : %s", response.status_code)
        if not vsp_found:
            self.update_vsp(id="")
            self.update_vsp(version="")
            self.update_vsp(status="")
            self.update_vsp(vendor_name="")
            self.update_vsp(csar_uuid="")
        return vsp_found

    def get_vsp_info(self):
        """
            Get vsp detail
        """
        vsp_list = self.get_vsp_list()
        for result in vsp_list['results']:
            if result['name'] == self.name:
                self.update_vsp(version=result["version"]["id"])
                self.update_vsp(status=result["status"])
                break
        return None

    def create_vsp(self, vendor):
        """
            Create vsp in SDC (only if it does not exist)
        """
        # we check if vsp exists or not, if not we create it
        create_vsp = False
        if not self.check_vsp_exists():
            try:
                url = SDC_URL2 + onap_utils.get_config(
                    "onap.sdc.create_vsp_url")
                data = self.get_sdc_vsp_payload(action="Create",
                                                vsp_name=self.name)
                data["vendorName"] = vendor.name
                data["vendorId"] = vendor.id
                data["name"] = self.name
                data = json.dumps(data)
                headers = SDC_HEADERS_DESIGNER
                self.__logger.debug("Create VSP URL : %s", url)
                self.__logger.debug("Create VSP payload : %s", data)
                response = requests.post(url, headers=headers,
                                         proxies=PROXY, verify=False,
                                         data=data)
                if response.status_code == 200:
                    create_vsp = True
                    create_response = response.json()
                    self.update_vsp(
                        status=create_response['version']['status'],
                        id=create_response['itemId'],
                        version=create_response['version']['id'],
                        vendor_name=vendor.name)
                    self.__logger.info(
                        "Create VSP code : %s", response.status_code)
                else:
                    self.__logger.error(
                        "Create VSP code : %s", response.status_code)
            except Exception as err:  # pylint: disable=broad-except
                self.__logger.error("Failed to create vsp: %s", err)
        else:
            self.__logger.info("VSP already exists")
        return create_vsp

    def new_vsp_version(self):
        """
            new_vsp_version in SDC
        """
        new_vsp_version = False
        try:
            old_str = onap_utils.get_config(
                "onap.sdc.new_vsp_version_url")
            new_str = old_str.replace("PPvsp_idPP", self.id)
            old_str = new_str
            new_str = old_str.replace("PPvsp_versionPP", self.version)
            url = SDC_URL2 + new_str
            data = self.get_sdc_vsp_payload(action="NewVersion")
            data = json.dumps(data)
            headers = SDC_HEADERS_DESIGNER
            self.__logger.debug("vsp_id : %s", self.id)
            self.__logger.debug("vsp_version : %s", self.version)
            self.__logger.debug("new_vsp_versionURL : %s", url)
            self.__logger.debug("new_vsp_version payload : %s", data)
            response = requests.post(url, headers=headers,
                                     proxies=PROXY, verify=False,
                                     data=data)
            if response.status_code == 200:
                new_vsp_version = True
                new_vsp_version_response = response.json()
                self.update_vsp(
                    status=new_vsp_version_response['status'],
                    version=new_vsp_version_response['id'])
                self.__logger.info(
                    "New version creation OK")
                self.__logger.debug(
                    "new_vsp_version version id : %s", self.version)
                self.__logger.debug(
                    "new_vsp_version status : %s", self.status)
            else:
                self.__logger.error(
                    "ERROR new_vsp_version code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to do new_vsp_version: %s", err)
        return new_vsp_version

    def upload_vsp(self, files):
        """
            upload zip for vsp
        """
        upload_vsp = False
        old_str = onap_utils.get_config("onap.sdc.upload_vsp_url")
        new_str = old_str.replace("PPvsp_idPP", self.id)
        old_str = new_str
        new_str = old_str.replace("PPvsp_versionPP", self.version)
        url = SDC_URL2 + new_str
        headers = SDC_UPLOAD_HEADERS
        self.__logger.debug("upload VSP URL : %s", url)
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     files=files)
            if response.status_code == 200:
                upload_vsp = True
                self.__logger.info(
                    "Upload VSP code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Upload VSP code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform file upload\
             for the vsp: %s", err)
        return upload_vsp

    def validate_vsp(self):
        """
            Validate vsp
        """
        validate_vsp = False
        old_str = onap_utils.get_config("onap.sdc.validate_vsp_url")
        new_str = old_str.replace("PPvsp_idPP", self.id)
        old_str = new_str
        new_str = old_str.replace("PPvsp_versionPP", self.version)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vsp_payload(action="Validate")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        response = requests.put(url,
                                headers=headers,
                                proxies=PROXY,
                                verify=False,
                                data=data)
        if response.status_code == 200:
            validate_vsp = True
            self.__logger.info(
                "Validate heat VSP code : %s", response.status_code)
        else:
            self.__logger.error(
                "Validate heat VSP code : %s", response.status_code)
            raise onap_test_exceptions.VspOnboardException
        return validate_vsp

    def commit_vsp(self):
        """
            commit vsp
        """
        commit_vsp = False
        old_str = onap_utils.get_config("onap.sdc.commit_vsp_url")
        new_str = old_str.replace("PPvsp_idPP", self.id)
        old_str = new_str
        new_str = old_str.replace("PPvsp_versionPP", self.version)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vsp_payload(action="Commit")
        data = json.dumps(data)
        self.__logger.debug(
            "Commit VSP url : %s", url)
        self.__logger.debug(
            "Commit VSP payload : %s", data)
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.put(url,
                                    headers=headers,
                                    proxies=PROXY,
                                    verify=False,
                                    data=data)
            if response.status_code == 200:
                commit_vsp = True
                self.__logger.info(
                    "Commit VSP code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Commit VSP code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform commit\
             vsp: %s", err)
        return commit_vsp

    def submit_vsp(self):
        """
            Submit vsp
        """
        submit_vsp = False
        old_str = onap_utils.get_config("onap.sdc.submit_vsp_url")
        new_str = old_str.replace("PPvsp_idPP", self.id)
        old_str = new_str
        new_str = old_str.replace("PPvsp_versionPP", self.version)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vsp_payload(action="Submit")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.put(url,
                                    headers=headers,
                                    proxies=PROXY, verify=False,
                                    data=data)
            if response.status_code == 200:
                submit_vsp = True
                self.__logger.info(
                    "Submit VSP code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Submit VSP code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform submit \
                              vsp: %s", err)
        return submit_vsp

    def create_package_vsp(self):
        """
            Create CSAR Package
        """
        create_package_vsp = False
        old_str = onap_utils.get_config("onap.sdc.create_package_vsp_url")
        new_str = old_str.replace("PPvsp_idPP", self.id)
        old_str = new_str
        new_str = old_str.replace("PPvsp_versionPP", self.version)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vsp_payload(action="Create_Package")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.put(url,
                                    headers=headers,
                                    proxies=PROXY, verify=False,
                                    data=data)
            if response.status_code == 200:
                create_package_vsp = True
                csar = response.json()
                self.update_vsp(csar_uuid=csar["packageId"])
                self.__logger.info(
                    "Create package VSP code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Create package VSP code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to perform create package \
                                vsp: %s", err)
        return create_package_vsp
