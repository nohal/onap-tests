#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=too-many-instance-attributes
#  pylint: disable=too-many-branches
#  pylint: disable=too-many-public-methods
"""Class to define Service onboarding"""

import datetime
import os
import logging
import json
import zipfile
import time
import requests
import urllib3

import onap_tests.utils.utils as onap_utils
import onap_tests.utils.exceptions as onap_test_exceptions
import onap_tests.components.servrelation as servrelation
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

PROXY = onap_utils.get_config("general.proxy")
SDC_URL = onap_utils.get_config("onap.sdc.url")
SDC_URL2 = onap_utils.get_config("onap.sdc.url2")
SDC_HEADERS = onap_utils.get_config("onap.sdc.headers")
SDC_HEADERS_DESIGNER = SDC_HEADERS.copy()
SDC_HEADERS_DESIGNER["USER_ID"] = onap_utils.get_config("onap.sdc.designer_id")
SDC_HEADERS_TESTER = SDC_HEADERS.copy()
SDC_HEADERS_TESTER["USER_ID"] = onap_utils.get_config("onap.sdc.tester_id")
SDC_HEADERS_GOV = SDC_HEADERS.copy()
SDC_HEADERS_GOV["USER_ID"] = onap_utils.get_config("onap.sdc.governor_id")
SDC_HEADERS_OP = SDC_HEADERS.copy()
SDC_HEADERS_OP["USER_ID"] = onap_utils.get_config("onap.sdc.operator_id")
SDC_UPLOAD_HEADERS = onap_utils.get_config("onap.sdc.download_headers")
AAI_URL = onap_utils.get_config("onap.aai.url")
AAI_HEADERS = onap_utils.get_config("onap.aai.headers")


class Service():
    """
        ONAP Service Object
        Used for SDC operations
    """
    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        # pylint: disable=invalid-name
        self.id = ""
        self.inv_uuid = ""
        self.unique_id = ""
        self.resource_version = ""
        if "service_name" in kwargs:
            self.name = kwargs['service_name']
        else:
            self.name = "ONAP-test-service"

        self.version = ""
        self.status = ""
        self.distri_status = ""

        if "customer_name" in kwargs:
            self.customer_name = kwargs['customer_name']
        else:
            self.customer_name = "generic"
        self.cloud_owner = onap_utils.get_config("openstack.cloud_owner")
        self.cloud_region_id = onap_utils.get_config("openstack.region_id")
        self.tenant_id = onap_utils.get_config("openstack.tenant_id")
        self.tenant_name = onap_utils.get_config("openstack.tenant_name")
        self.servrelation = servrelation.Servrelation()

    def update_service(self, **kwargs):
        """
            Update Service values
        """
        if "id" in kwargs:
            self.id = kwargs['id']
        if "inv_uuid" in kwargs:
            self.inv_uuid = kwargs['inv_uuid']
        if "unique_id" in kwargs:
            self.unique_id = kwargs['unique_id']
        if "resource_version" in kwargs:
            self.resource_version = kwargs['resource_version']
        if "name" in kwargs:
            self.name = kwargs['name']
        if "version" in kwargs:
            self.version = kwargs['version']
        if "status" in kwargs:
            self.status = kwargs['status']

        if "distri_status" in kwargs:
            self.distri_status = kwargs['distri_status']
        if "customer_name" in kwargs:
            self.customer_name = kwargs['customer_name']

    def get_sdc_service_payload(self, **kwargs):
        """
            Build SDC service payload
        """
        try:
            payload = {}
            if kwargs['action'] == "Create":
                payload = onap_utils.get_config(
                    "service_payload.service_create_data")
                if "service_name" in kwargs:
                    payload['tags'][0] = kwargs['service_name']
                    payload['description'] = kwargs['service_name']
                    payload['name'] = kwargs['service_name']
                else:
                    payload['tags'][0] = "ONAP-test-service"
                    payload['description'] = "ONAP-test-service"
                    payload['name'] = "ONAP-test-service"
            if kwargs['action'] == "Add_vf_to_service":
                payload = onap_utils.get_config(
                    "service_payload.service_add_vf_data")
                if "vf_name" and "vf_unique_id" in kwargs:
                    payload['name'] = kwargs['vf_name']
                    dtime = datetime.datetime.now()
                    payload['uniqueId'] = kwargs['vf_unique_id'] + \
                                          dtime.strftime("%s")
                    payload['componentUid'] = kwargs['vf_unique_id']
                    payload['posX'] = kwargs['posx']
                    payload['posY'] = kwargs['posy']
                # To be modified once version will be managed by all functions
                if "vf_version" in kwargs:
                    payload['componentVersion'] = kwargs['vf_version']
            if kwargs['action'] == "Checkin":
                payload = onap_utils.get_config(
                    "service_payload.service_checkin_data")
            if kwargs['action'] == "Submit_for_testing":
                payload = onap_utils.get_config(
                    "service_payload.service_submit_for_testing_data")
            if kwargs['action'] == "Start_certification":
                payload = onap_utils.get_config(
                    "service_payload.service_start_certif_data")
            if kwargs['action'] == "Certify":
                payload = onap_utils.get_config(
                    "service_payload.service_certify_data")
            if kwargs['action'] == "Approve":
                payload = onap_utils.get_config(
                    "service_payload.service_approve_data")
            if kwargs['action'] == "Distribute":
                payload = onap_utils.get_config(
                    "service_payload.service_distribute_data")

        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_aai_service_payload(self, **kwargs):
        """
            Build AAI service payload
        """
        try:
            payload = {}
            if kwargs['action'] == "declare_service_in_aai":
                payload = onap_utils.get_config(
                    "service_payload.declare_service_in_aai_data")
                if "service_name" in kwargs:
                    payload['service-id'] = kwargs['service_invUUID']
                    payload['service-description'] = kwargs['service_name']
                else:
                    payload['service-id'] = kwargs['service_invUUID']
                    payload['service-description'] = "ONAP-test-service"

            if kwargs['action'] == "add_relations":
                payload = onap_utils.get_config(
                    "service_payload.add_relations_data")
                payload['service-type'] = kwargs['service_name']
                relationship_datas = payload['relationship-list']['relation\
ship'][0]["relationship-data"]
                for relationship_data in relationship_datas:
                    if relationship_data['relationship-key'] == "cloud-region\
.cloud-region-id":
                        relationship_data['relationship\
-value'] = self.cloud_region_id
                    if relationship_data['relationship\
-key'] == "tenant.tenant-id":
                        relationship_data['relationship\
-value'] = self.tenant_id
                related_to_property = payload['relationship\
-list']['relationship'][0]["related-to-property"]
                related_to_property[0]['property-value'] = self.tenant_name
                related_link = str(payload['relationship\
-list']['relationship'][0]["related-link"])
                related_link = related_link.replace(
                    "PPcloud_region_idPP", self.cloud_region_id)
                related_link = related_link.replace(
                    "PPtenant_idPP", self.tenant_id)
                payload['relationship-list']['relationship'][0]["related\
-link"] = related_link

        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_service_list(self):
        """
            Get service list
        """
        url = SDC_URL + onap_utils.get_config(
            "onap.sdc.list_service_url")
        service_list = {}
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            service_list = response.json()
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform List service: %s", err)
        return service_list

    def get_service_list_in_aai(self):
        """
            Get service list in AAI
        """
        url = AAI_URL + onap_utils.get_config(
            "onap.aai.list_service_url")
        service_list = {}
        headers = AAI_HEADERS
        try:
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            service_list = response.json()
            self.__logger.info(
                "GET Service list from AAI code : %s", response.status_code)
            if response.status_code == 404:
                service_list = []
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform List service in AAI: %s", err)
        return service_list

    def get_service_metadata(self):
        """
            Get service metadata to get service unique_id when new version
        """
        service_unique_id_updated = False
        old_str = onap_utils.get_config("onap.sdc.metadata_service_url")
        new_str = old_str.replace("PPservice_unique_idPP", self.unique_id)
        url = SDC_URL2 + new_str
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.get(url, headers=headers,
                                    proxies=PROXY, verify=False)
            if response.status_code == 200:
                service_unique_id_updated = True
                service_meta = response.json()
                # only working with 1.0 version
                self.update_service(
                    unique_id=service_meta["metadata"]["allVersions"]["1.0"])
                self.__logger.info(
                    "Get service metadata code : %s", response.status_code)
                self.__logger.info(
                    "Service unique Id : %s", self.unique_id)
            else:
                self.__logger.error(
                    "Get service metadata code : %s", response.status_code)
                self.__logger.error(
                    "Service unique Id : %s", self.unique_id)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error("Failed to get service metadata: %s", err)
        return service_unique_id_updated

    def check_service_exists(self):
        """
            Check if provided service exists in service list
        """
        service_list = self.get_service_list()
        service_found = False
        if service_list:
            for result in service_list:
                if result['name'] == self.name:
                    service_found = True
                    self.update_service(id=result["uuid"])
                    self.update_service(version=result["version"])
                    self.update_service(status=result["lifecycleState"])
                    self.update_service(
                        distri_status=result["distributionStatus"])
                    self.update_service(inv_uuid=result["invariantUUID"])
        urlpart = onap_utils.get_config(
            "onap.sdc.get_resource_list_url")

        url = SDC_URL2 + urlpart
        headers = SDC_HEADERS_DESIGNER
        response = requests.get(url, headers=headers,
                                proxies=PROXY, verify=False)
        if response.status_code == 200:
            result = response.json()
            for resource in result["services"]:
                if resource["uuid"] == self.id:
                    self.__logger.debug(
                        "Service resource %s found in service list",
                        resource["name"])
                    self.update_service(unique_id=resource["uniqueId"])
        else:
            self.__logger.error(
                "Get resource list status code : %s", response.status_code)

        self.__logger.debug("service found parameter %s", service_found)
        return service_found

    def check_service_exists_in_aai(self):
        """
            Check if provided service exists in AAI service list
        """
        self.__logger.info("check if %s exists in AAI", self.name)
        service_list = self.get_service_list_in_aai()
        service_found_in_aai = False
        if service_list:
            for result in service_list['service']:
                if result['service-description'] == self.name:
                    service_found_in_aai = True
                    self.update_service(
                        resource_version=result["resource-version"])
        return service_found_in_aai

    def create_service(self):
        """
            Create service in SDC (only if it does not exist)
        """
        # we check if service exists or not, if not we create it
        create_service = False
        if not self.check_service_exists():
            try:
                url = SDC_URL2 + onap_utils.get_config(
                    "onap.sdc.create_service_url")
                data = self.get_sdc_service_payload(action="Create",
                                                    service_name=self.name)
                data = json.dumps(data)
                headers = SDC_HEADERS_DESIGNER
                response = requests.post(url, headers=headers,
                                         proxies=PROXY, verify=False,
                                         data=data)
                if response.status_code == 201:
                    create_service = True
                    service = response.json()
                    self.update_service(id=service["uuid"])
                    self.update_service(unique_id=service["uniqueId"])
                    self.__logger.info(
                        "Create service code : %s", response.status_code)
                    self.__logger.info(
                        "Service unique Id : %s", self.unique_id)
                else:
                    self.__logger.error(
                        "Create service code : %s", response.status_code)
                    self.__logger.error(
                        "Service unique Id : %s", self.unique_id)
            except Exception as err:  # pylint: disable=broad-except
                self.__logger.error(
                    "Failed to perform Create service: %s", err)
        return create_service

    def add_vf_to_service(self, vfunc):
        """
            Add VF to service in SDC
        """
        add_vf_to_service = False
        old_str = onap_utils.get_config("onap.sdc.add_vf_to_service_url")
        new_str = old_str.replace("PPservice_uuidPP", self.unique_id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_service_payload(action="Add_vf_to_service",
                                            vf_name=vfunc.name,
                                            vf_unique_id=vfunc.unique_id,
                                            vf_version=vfunc.version,
                                            posx=vfunc.posx,
                                            posy=vfunc.posy)
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        self.__logger.debug("Add VF to existing service url : %s", url)
        self.__logger.debug("Add VF to existing service headers : %s", headers)
        self.__logger.debug("Add VF to existing service data : %s", data)
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 201:
                add_vf_to_service = True
                self.__logger.info(
                    "Add VF to Service code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Add VF to Service code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform Add VF to service: %s", err)
        return add_vf_to_service

    def get_component_instances(self, vf_name, vflink_name, relation_type):
        """
            Get inputs from get service componentInstances
        """
        get_component_instances = ""
        url = SDC_URL2 + onap_utils.get_config(
            "onap.sdc.get_services_componentInstances_url").\
            replace("PPservice_uuidPP", self.unique_id)
        headers = SDC_HEADERS_DESIGNER
        self.__logger.debug("Get service componentInstances url : %s", url)
        self.__logger.debug("Get service componentInstances headers: %s", \
            headers)
        try:
            response = requests.get(url,
                                    headers=headers,
                                    proxies=PROXY,
                                    verify=False)
            if response.status_code == 200:
                self.__logger.info(
                    "Get Service component instances information : %s", \
                         response.status_code)
                inputs = response.json()
            else:
                self.__logger.error(
                    "Get Service component instances information : %s", \
                    response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to get service instance components answer: %s", err)
        try:
            payload = self.servrelation.\
                set_payload_relation(inputs,
                                     vf_name,
                                     vflink_name,
                                     relation_type)
            get_component_instances = json.dumps(payload)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to get service instance components: %s", err)
        return get_component_instances

    def create_relation(self, vf_name, idx, typevf):
        """
            Create config relation with VFs
        """
        # Get the list of relations to create
        service_params = onap_utils.get_service_custom_config(
            self.name)
        params = service_params[typevf][idx]
        if 'relationtype' in params and 'relationwithvnf' in params:
            relationtype = service_params[typevf][idx]['relationtype']
            self.__logger.debug(
                "Create relations between VFs relationtype : %s",
                relationtype)
            for index in service_params[typevf][idx]['relationwithvnf']:
                vflink_name = service_params['vnfs'][index]['vnf_name'] + "_VF"
                self.__logger.debug("Create relations Index value : %s",
                                    idx)
                self.__logger.info("Create relations from VF : %s",
                                   vf_name)
                self.__logger.debug(
                    "Create relations with VF : %s",
                    vflink_name)
                old_str = onap_utils.get_config(
                    "onap.sdc.create_relation_between_vf_url")
                url = SDC_URL2 + old_str.replace(
                    "PPservice_uuidPP",
                    self.unique_id)
                data = self.get_component_instances(vf_name,
                                                    vflink_name,
                                                    relationtype)
                self.__logger.debug(
                    "Create relations between VFs url : %s", url)
                self.__logger.debug(
                    "Create relations between VFs data: %s", data)
                try:
                    response = requests.post(url,
                                             headers=SDC_HEADERS_DESIGNER,
                                             proxies=PROXY,
                                             verify=False,
                                             data=data)
                    if response.status_code == 200:
                        self.__logger.info(
                            "Associate VFs in service : %s",
                            response.status_code)
                    else:
                        self.__logger.error(
                            "Associate VFs in service : %s",
                            response.status_code)
                except Exception as err:  # pylint: disable=broad-except
                    self.__logger.error(
                        "Failed to associate VFs in service: %s", err)

    def upgrade_vf_to_service(self, vfunc):
        """
            Upgrade VF to service in SDC
        """
        upgrade_vf_to_service = False
        old_str = onap_utils.get_config("onap.sdc.upgrade_vf_to_service_url")
        new_str = old_str.replace("PPservice_uuidPP", self.unique_id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_service_payload(action="Upgrade_vf_to_service",
                                            vf_name=vfunc.name,
                                            vf_unique_id=vfunc.unique_id,
                                            posx=vfunc.posx)
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 201:
                upgrade_vf_to_service = True
                self.__logger.info(
                    "Upgrade VF to Service code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Upgrade VF to Service code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform Upgrade VF to service: %s", err)
        return upgrade_vf_to_service

    def get_service_info(self, service):
        """
            Get service detail
        """
        service_list = self.get_service_list()
        for result in service_list:
            if result['name'] == service['name']:
                service = result
                break
        return service

    def checkin_service(self):
        """
            Checkin service
        """
        checkin_service = False
        old_str = onap_utils.get_config("onap.sdc.checkin_service_url")
        new_str = old_str.replace("PPservice_uuidPP", self.id)
        url = SDC_URL + new_str
        data = self.get_sdc_service_payload(action="Checkin")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 201:
                checkin_service = True
                self.__logger.info(
                    "Checkin service code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Checkin service code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform Checkin service: %s", err)
        return checkin_service

    def checkout_service(self):
        """
            Checkout service
        """
        checkout_service = False
        old_str = onap_utils.get_config("onap.sdc.checkout_service_url")
        new_str = old_str.replace("PPservice_uuidPP", self.unique_id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_service_payload(action="Checkout")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        self.__logger.debug("Checkout service url : %s", url)
        self.__logger.debug("Checkout service headers : %s", headers)
        self.__logger.debug("Checkout service data : %s", data)
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 200:
                checkout_service = True
                service_meta = response.json()
                self.__logger.debug("Checkout service answer : %s",
                                    service_meta)
                # only working with 1.0 version
                self.update_service(
                    unique_id=service_meta["uniqueId"])
                self.__logger.info(
                    "Service unique Id : %s", self.unique_id)
                self.__logger.info(
                    "Checkout service code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Checkout service code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform Checkout service: %s", err)
        return checkout_service

    def submit_for_testing_service(self):
        """
            submit_for_testing service
        """
        submit_for_testing_service = False
        old_str = onap_utils.get_config(
            "onap.sdc.submit_for_testing_service_url")
        new_str = old_str.replace("PPservice_uuidPP", self.id)
        url = SDC_URL + new_str
        data = self.get_sdc_service_payload(action="Submit_for_testing")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        self.__logger.debug(
            "Submit service url : %s", url)
        self.__logger.debug(
            "Submit service headers : %s", headers)
        self.__logger.debug(
            "Submit service data : %s", data)
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 201:
                submit_for_testing_service = True
                self.__logger.info(
                    "Submit service code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Submit service code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform Submit service: %s", err)
        return submit_for_testing_service

    def start_certif_service(self):
        """
            start_certif service
        """
        start_certif_service = False
        old_str = onap_utils.get_config("onap.sdc.start_certif_service_url")
        new_str = old_str.replace("PPservice_uuidPP", self.id)
        url = SDC_URL + new_str
        data = self.get_sdc_service_payload(action="Start_certification")
        data = json.dumps(data)
        headers = SDC_HEADERS_TESTER
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 201:
                start_certif_service = True
                self.__logger.info(
                    "Start certif service code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Start certif service code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform Start certification service: %s", err)
        return start_certif_service

    def certify_service(self):
        """
            certify service
        """
        certify_service = False
        old_str = onap_utils.get_config("onap.sdc.certify_service_url")
        new_str = old_str.replace("PPservice_uuidPP", self.id)
        url = SDC_URL + new_str
        data = self.get_sdc_service_payload(action="Certify")
        data = json.dumps(data)
        headers = SDC_HEADERS_TESTER
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 201:
                certify_service = True
                self.__logger.info(
                    "Certify service code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Certify service code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform Certify service: %s", err)
        return certify_service

    def approve_service(self):
        """
            approve service
        """
        approve_service = False
        old_str = onap_utils.get_config("onap.sdc.approve_service_url")
        new_str = old_str.replace("PPservice_uuidPP", self.unique_id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_service_payload(action="Approve")
        data = json.dumps(data)
        headers = SDC_HEADERS_GOV
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 200:
                approve_service = True
                self.__logger.info(
                    "Approve service code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Approve service code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform Approve service: %s", err)
        return approve_service

    def distribute_service(self):
        """
            distribute service
        """
        distribute_service = False
        old_str = onap_utils.get_config("onap.sdc.distribute_service_url")
        new_str = old_str.replace("PPservice_uuidPP", self.unique_id)
        url = SDC_URL2 + new_str
        data = self.get_sdc_service_payload(action="Distribute")
        data = json.dumps(data)
        headers = SDC_HEADERS_OP
        try:
            response = requests.post(url,
                                     headers=headers,
                                     proxies=PROXY,
                                     verify=False,
                                     data=data)
            if response.status_code == 200:
                distribute_service = True
                self.__logger.info(
                    "Distribute service code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Distribute service code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform Distribute service: %s", err)
        return distribute_service

    def download_tosca_service(self, **kwargs):
        """
            download service CSAR file and save/store service template
        """
        if "service_uuid" in kwargs:
            self.id = kwargs['service_uuid']
        if "service_name" in kwargs:
            self.name = kwargs['service_name']
        url = (SDC_URL +
               onap_utils.get_config(
                   "onap.sdc.download_tosca_service_url").replace(
                       "PPservice_uuidPP",
                       self.id))
        self.__logger.debug("download CSAR URL : %s", url)
        try:
            response = requests.get(url,
                                    headers=SDC_UPLOAD_HEADERS,
                                    proxies=PROXY,
                                    verify=False,
                                    stream=True)
            if response.status_code == 200:
                path = os.path.dirname(os.path.abspath(__file__)).replace(
                    "components", "templates/tosca_files/")
                csar_filename = "service-" + self.name + "-csar.csar"
                self.__logger.debug(
                    "CSAR filename we are saving downloaded data : %s",
                    (path + csar_filename))
                with open((path + csar_filename), 'wb') as csar_file:
                    for chunk in response.iter_content(chunk_size=128):
                        csar_file.write(chunk)
                with zipfile.ZipFile(path + csar_filename) as myzip:
                    for name in myzip.namelist():
                        if (name[-13:] == "-template.yml" and
                                name[:20] == "Definitions/service-"):
                            service_template = name
                    with myzip.open(service_template) as file1:
                        with open(path + service_template[12:], 'wb') as file2:
                            file2.write(file1.read())
                self.__logger.info(
                    "Download CSAR code : %s", response.status_code)
            else:
                self.__logger.error(
                    "Download CSAR code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform Download CSAR : %s", err)
            raise onap_test_exceptions.CsarDownloadException

    def declare_service_in_aai(self):
        """
            declare service in AAI (subscription notion)
        """
        declare_service_in_aai = False
        old_str = onap_utils.get_config(
            "onap.aai.declare_service_in_aai_url")
        new_str = old_str.replace(
            "PPservice_invUUIDPP", self.inv_uuid)
        url = AAI_URL + new_str
        data = self.get_aai_service_payload(
            action="declare_service_in_aai",
            service_name=self.name,
            service_invUUID=self.inv_uuid)
        data = json.dumps(data)
        headers = AAI_HEADERS
        try:
            response = requests.put(url,
                                    headers=headers,
                                    proxies=PROXY,
                                    verify=False,
                                    data=data)
            if response.status_code == 201:
                declare_service_in_aai = True
                self.__logger.info(
                    "declare_service_in_aai code : %s",
                    response.status_code)
            else:
                self.__logger.error(
                    "declare_service_in_aai code : %s",
                    response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform declare_service_in_aai : %s", err)
        return declare_service_in_aai

    def add_relations(self):
        """
            Add Customer, Service and Tenant relations
        """
        add_relations = False
        old_str = onap_utils.get_config(
            "onap.aai.add_relations_url")
        new_str1 = old_str.replace("PPcustomer_namePP", self.customer_name)
        new_str2 = new_str1.replace("PPservice_namePP", self.name)
        new_str3 = new_str2.replace(
            "PPservice_resource_versionPP", self.resource_version)
        url = AAI_URL + new_str3
        data = self.get_aai_service_payload(
            action="add_relations",
            service_name=self.name,
            cloud_region_id=self.cloud_region_id,
            tenant_id=self.tenant_id,
            tenant_name=self.tenant_name)
        data = json.dumps(data)
        headers = AAI_HEADERS
        self.__logger.debug("add_relations service url : %s", url)
        self.__logger.debug("add_relations service headers : %s", headers)
        self.__logger.debug("add_relations service data : %s", data)
        try:
            response = requests.put(url,
                                    headers=headers,
                                    proxies=PROXY,
                                    verify=False,
                                    data=data)
            if response.status_code == 201:
                add_relations = True
                self.__logger.info(
                    "add_relations code : %s", response.status_code)
            else:
                self.__logger.error(
                    "add_relations code : %s", response.status_code)
        except Exception as err:  # pylint: disable=broad-except
            self.__logger.error(
                "Failed to perform add_relations : %s", err)
        return add_relations

    def get_sdc_headers(self, oldkey, newkey, user):
        """
            Util function to modify headers
        """
        # get SDC header and replace the header by the one specified in param
        sdc_new_headers = SDC_HEADERS
        self.__logger.debug("old headers: %s", sdc_new_headers)
        if oldkey in sdc_new_headers:
            sdc_new_headers[newkey] = sdc_new_headers.pop(oldkey)
        sdc_new_headers[newkey] = user
        self.__logger.debug("header field modified %s.", sdc_new_headers)
        return sdc_new_headers

    def get_distribution_id(self, service_id):
        """
            Get distribution ID based on Service UUID
        """
        distribution_id = None
        url = (SDC_URL2 + "/sdc1/feProxy/rest/v1/catalog/services/" +
               service_id + "/distribution")
        self.__logger.debug("SDC request: %s", url)
        headers = self.get_sdc_headers("USER_ID", "user_id", "op0001")
        response = requests.get(url, headers=headers,
                                proxies=PROXY, verify=False)
        self.__logger.info("SDC get distribution ID request: %s",
                           response.text)
        res = response.json()
        distribution_id = (
            res['distributionStatusOfServiceList'][0]['distributionID'])
        return distribution_id

    def is_service_distributed(self, distribution_id):
        """
            Check if the Service has been properly distributed
            in SO, AAI and SDNC
        """
        url = (SDC_URL2 +
               "/sdc1/feProxy/rest/v1/catalog/services/distribution/" +
               distribution_id)
        request_completed = False
        nb_try = 0
        nb_try_max = 5
        headers = self.get_sdc_headers("USER_ID", "user_id", "op0001")
        components = ["mso", "sdnc", "aai"]
        status = {'aai': False, 'mso': False, 'sdnc': False}
        while request_completed is False and nb_try < nb_try_max:
            response = onap_utils.get_simple_request(url,
                                                     headers,
                                                     PROXY)
            self.__logger.info("SDC: looking for %s distribution status....",
                               distribution_id)
            # self.__logger.debug("SDC answer: %s", response)
            distrib_list = response['distributionStatusList']
            self.__logger.debug("distrib_list = %s", distrib_list)
            for elt in distrib_list:
                for i in components:
                    if (i in elt['omfComponentID'] and
                            'DOWNLOAD_OK' in elt['status']):
                        status[i] = True
                        self.__logger.info("Service distributed in %s", i)
            if status['aai'] and status['mso'] and status['sdnc']:
                self.__logger.info("fully distributed")
                return True
            time.sleep(60)
            nb_try += 1

        if request_completed is False:
            self.__logger.info("Distribution not seen completed")
            raise onap_test_exceptions.SdcRequestException
