#!/usr/bin/env python

import logging
import time

import onap_tests.scenario.e2e as e2e
from xtesting.core import testcase


class FreeradiusNbi(testcase.TestCase):
    """
    Onboard then instantiate a simple Freeradius VNF though ONAP
    """
    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """ Init BasicVM """
        if "case_name" not in kwargs:
            kwargs["case_name"] = 'basic_vm'
        super(FreeradiusNbi, self).__init__(**kwargs)
        self.__logger.debug("Freeradius init started")
        self.test = e2e.E2E(service_name='freeradius', nbi=True)
        self.start_time = None
        self.stop_time = None
        self.result = 0

    def run(self):
        """ run onap_tests with freeradius VM """
        self.start_time = time.time()
        self.__logger.debug("start time")
        try:
            self.test.execute()
            self.__logger.info("Freeradius VNF with NBI successfully created")
            # We consider that the cleaning is part of the case
            # not a resouce cleaning step
            self.test.clean()
            self.__logger.info("Freeradius VNF with NBI successfully created")
            self.result = 100
            self.stop_time = time.time()
            return testcase.TestCase.EX_OK
        except Exception:  # pylint: disable=broad-except
            self.result = 0
            self.stop_time = time.time()
            self.__logger.error("Freeradius with NBI test failed.")
            return testcase.TestCase.EX_TESTCASE_FAILED

    def clean(self):
        """ Clean VNF """
        pass
