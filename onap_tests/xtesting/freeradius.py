#!/usr/bin/env python

import logging
import time

import onap_tests.scenario.e2e as e2e
from xtesting.core import testcase


class Freeradius(testcase.TestCase):
    """
    Onboard then instantiate a simple Freeradius VNF though ONAP
    """
    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """ Init BasicVM """
        if "case_name" not in kwargs:
            kwargs["case_name"] = 'basic_vm'
        super(Freeradius, self).__init__(**kwargs)
        self.__logger.debug("Freeradius init started")
        self.test = e2e.E2E(service_name='freeradius')
        self.start_time = None

    def run(self):
        """ run onap_tests with freeradius VM """
        self.start_time = time.time()
        self.__logger.debug("start time")
        self.test.execute()
        self.__logger.info("Freeradius VNF successfully created")
        self.result = 100
        self.stop_time = time.time()
        return testcase.TestCase.EX_OK

    def clean(self):
        """ Clean VNF """
        self.test.clean()
        self.__logger.debug("VNF cleaned")
