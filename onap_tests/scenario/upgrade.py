#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
import logging
import os
import os.path
import copy

import onap_tests.components.vendor as vendor
import onap_tests.components.vsp as vsp
import onap_tests.components.vf as vfunc
import onap_tests.components.service as service

import onap_tests.utils.utils as onap_utils

PROXY = onap_utils.get_config("general.proxy")


class Upgrade():
    """
    Upgrade classe to perform SDC operations
    """
    __logger = logging.getLogger(__name__)
    logging.basicConfig(format='%(asctime)s %(message)s',
                        level=logging.DEBUG)

    def __init__(self, **kwargs):
        "Initialize Upgrade object."
        super(Upgrade, self).__init__()
        self.vendor = vendor.Vendor(**kwargs)
        self.vsp = vsp.VSP(**kwargs)
        self.vsp_list = []
        self.vfunc = vfunc.VF(**kwargs)
        self.vfunc_list = []
        self.service = service.Service(**kwargs)

    def check_vendor_prerequisites(self):
        # 0) Process pre-requisites :
        #       vendor must exists
        #       vendor information must be loaded
        upgrade_prerequisites = False
        if self.vendor.check_vendor_exists():
            self.__logger.info("Check and update vendor info")
            upgrade_prerequisites = True
        else:
            self.__logger.info(
                "Vendor does not exists : stop VSP upgrade")
        return upgrade_prerequisites

    def upgrade_vsp(self):
        # upgrade an already onboarded VSP
        service_params = onap_utils.get_service_custom_config(
            self.service.name)
        for idx, vnf in enumerate(service_params['vnfs']):
            self.vsp.update_vsp(name=vnf['vnf_name']+"_VSP")
            self.__logger.info("Starting upgrade for VSP %s %s",
                               self.vsp.name, idx)
            self.vsp.check_vsp_exists()
            # if self.check_vendor_prerequisites():

            # 1) create new VSP version
            self.vsp.new_vsp_version()

            # 2) upload new heat files
            heat_zip_file = vnf['heat_files_to_upload']
            if os.path.isfile("onap_tests/templates/\
heat_files/" + heat_zip_file):
                self.__logger.info(
                    "upload new Heat files to VSP %s", self.vsp.name)
                files = {'upload': open(
                    "onap_tests/templates/\
heat_files/" + heat_zip_file, 'rb')}
                self.vsp.upload_vsp(files)
                self.__logger.info("Check and update VSP")
                self.vsp.check_vsp_exists()
            else:
                self.__logger.error("Heat zip File is missing")

            # 3) Validate
            # verifier les différents états
            self.__logger.info("validate Heat files")
            self.vsp.validate_vsp()
            self.__logger.info("Check and update VSP")
            self.vsp.check_vsp_exists()

            # 4) Commit
            self.__logger.info("vsp commit")
            self.vsp.commit_vsp()
            self.__logger.info("Check and update VSP")
            self.vsp.check_vsp_exists()

            # 5) Submit
            self.__logger.info("vsp submit")
            self.vsp.submit_vsp()
            self.__logger.info("Check and update VSP")
            self.vsp.check_vsp_exists()

            # 6) Create package
            self.__logger.info("Create CSAR package")
            self.vsp.create_package_vsp()
            self.__logger.info("Check and update VSP")
            self.vsp.check_vsp_exists()
            self.vsp_list.append(copy.copy(self.vsp))
