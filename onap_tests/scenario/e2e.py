#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""
ONAP End to End class
"""
import argparse
import logging
import logging.config
import pkg_resources

import onap_tests.scenario.onboard as onboard
import onap_tests.scenario.instantiate as instantiate
import onap_tests.utils.exceptions as onap_test_exceptions
import onap_tests.utils.utils as onap_utils

PROXY = onap_utils.get_config("general.proxy")


class E2E():
    """ E2E class to chain onboarding then instantiation """
    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Initialize E2E object."""
        super(E2E, self).__init__()

        # by default we consider that we need
        # - the service name
        # - the vendor name
        # - the customer name
        # in onboard only service is mandatory
        # default values are used if not specified
        try:
            self.service_name = kwargs["service_name"]
            self.__logger.info("VNF %s: test initialized", self.service_name)
            self.instantiate_vnf = None
            self.nbi_option = False
            self.vendor_name = "Generic-Vendor"
            self.customer_name = "generic"
            if "nbi" in kwargs:
                self.nbi_option = kwargs["nbi"]
            if "vendor_name" in kwargs:
                self.vendor_name = kwargs["vendor_name"]
            if "customer_name" in kwargs:
                self.customer_name = kwargs["customer_name"]
        except KeyError:
            raise onap_test_exceptions.VnfInitException

    def execute(self):
        """ Chain onboarding and instantiation"""
        # 1 onboard the Vnf
        # if already done, nothing will be done
        self.__logger.info("Start onboarding")
        onboard_vnf = onboard.Onboard(service_name=self.service_name,
                                      vendor_name=self.vendor_name,
                                      customer_name=self.customer_name)
        onboard_vnf.onboard_vendor()
        onboard_vnf.onboard_vsp()
        onboard_vnf.onboard_vf()
        onboard_vnf.onboard_service()
        self.__logger.info("Onboarding OK")
        self.__logger.info("Wait for distribution")

        # 2: instantiate
        self.__logger.info("Start instantiation")
        self.__logger.info("NBI option: %s", self.nbi_option)
        self.instantiate_vnf = instantiate.Instantiate(
            service_name=self.service_name,
            nbi=self.nbi_option)
        self.instantiate_vnf.instantiate()

    def clean(self):
        """Clean only the Instantiation"""
        self.instantiate_vnf.clean()


def main():
    """Entry point"""
    logging.config.fileConfig(pkg_resources.resource_filename(
        'onap_tests', 'onap-conf/logging.ini'))
    logging.captureWarnings(True)
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '-s', '--service',
            help='service name: ims, ubuntu16, freeradius, vfw')
        parser.add_argument(
            '-c', '--customer', default='Generic-Vendor',
            help='customer name')
        parser.add_argument(
            '-v', '--vendor', default='generic',
            help='vendor name')
        parser.add_argument('-n', '--nbi', default=False,
                            action="store_true",
                            help='Use NBI if set')
        args = parser.parse_args()
        e2e = E2E(service_name=args.service,
                  nbi=args.nbi,
                  customer_name=args.customer,
                  vendor_name=args.vendor)
        e2e.execute()
        e2e.clean()
        return True
    except Exception:  # pylint: disable=broad-except
        return False

if __name__ == "__main__":
    main()
