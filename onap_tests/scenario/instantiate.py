#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=duplicate-code
#  pylint: disable=missing-docstring
import logging
import time

import onap_tests.components.aai as aai
import onap_tests.components.so as so
import onap_tests.components.sdnc as sdnc
import onap_tests.components.nbi as nbi
import onap_tests.utils.stack_checker as sc
import onap_tests.utils.utils as onap_utils
import onap_tests.utils.exceptions as onap_test_exceptions

PROXY = onap_utils.get_config("general.proxy")


class Instantiate():
    """
    VNF: Class to automate the instantiation of a VNF
    It is assumed that the Design phase has been already done
    The yaml template is available and stored in the template directory
    TODO: automate the design phase
    """
    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Initialize Solution object."""
        super(Instantiate, self).__init__()
        self.vnf_config = {}
        self.components = {}
        if "service_name" not in kwargs:
            # by convention is VNF is not precised we set ubuntu16
            kwargs["service_name"] = "ubuntu16"
        self.vnf_config["vnf"] = kwargs["service_name"]
        self.vnf_config["nbi"] = False
        if "nbi" in kwargs:
            self.vnf_config["nbi"] = kwargs["nbi"]

        # can be useful to destroy resources, sdnc module name shall be given
        if "sdnc_vnf_name" in kwargs:
            self.vnf_config["sdnc_vnf_name"] = kwargs["sdnc_vnf_name"]
            # Random part = 6 last char of the the vnf name
            self.vnf_config["random_string"] = kwargs["sdnc_vnf_name"][-6:]
        else:
            self.vnf_config["random_string"] = (
                onap_utils.random_string_generator())
            self.vnf_config["sdnc_vnf_name"] = (
                onap_utils.get_config("onap.service.name") + "_" +
                kwargs["service_name"] + "_" +
                self.vnf_config["random_string"])

        vnf_list = list(onap_utils.get_template_param(
            self.vnf_config["vnf"],
            "topology_template.node_templates"))
        vf_module_list = list(onap_utils.get_template_param(
            self.vnf_config["vnf"],
            "topology_template.groups"))
        # Class attributes for instance, vnf and module VF
        self.service_infos = {}
        self.vnf_infos = {'list': vnf_list}
        self.module_infos = {'list': vf_module_list}

        # retrieve infos from the configuration files
        self.set_service_instance_var()
        self.set_vnf_var()
        self.set_module_var()
        self.set_onap_components()

    def set_service_instance_var(self):
        """
        set service instance variables from the config file
        """
        self.vnf_config["vnf_name"] = onap_utils.get_template_param(
            self.vnf_config["vnf"], "metadata.name")
        self.vnf_config["invariant_uuid"] = onap_utils.get_template_param(
            self.vnf_config["vnf"], "metadata.invariantUUID")
        self.vnf_config["uuid"] = onap_utils.get_template_param(
            self.vnf_config["vnf"], "metadata.UUID")

    def set_vnf_var(self):
        """
        set vnf variables from the config file
        """
        for i, elt in enumerate(self.vnf_infos['list']):
            vnf_config = {}
            self.__logger.info("get VNF %s info", elt)
            vnf_config["vnf_customization_name"] = elt
            vnf_config["vnf_model_name"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                vnf_config["vnf_customization_name"] + ".metadata.name")
            vnf_config["vnf_invariant_id"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                vnf_config["vnf_customization_name"] +
                ".metadata.invariantUUID")
            vnf_config["vnf_version_id"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                vnf_config["vnf_customization_name"] + ".metadata.UUID")
            vnf_config["vnf_version"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                vnf_config["vnf_customization_name"] + ".metadata.version")
            vnf_config["vnf_customization_id"] = (
                onap_utils.get_template_param(
                    self.vnf_config["vnf"],
                    "topology_template.node_templates." +
                    vnf_config["vnf_customization_name"] +
                    ".metadata.customizationUUID"))
            vnf_config["vnf_type"] = list(onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.groups"))[i]
            vnf_config["vnf_generic_name"] = (
                self.vnf_config["vnf_name"] + "-service-instance-" +
                self.vnf_config["random_string"])
            vnf_config["vnf_generic_type"] = (
                self.vnf_config["vnf_name"] + "/" +
                vnf_config["vnf_customization_name"])
            self.vnf_config[elt] = vnf_config
            self.vnf_config[elt]["vnf_parameters"] = \
                onap_utils.get_vnf_parameters(self.vnf_config["vnf"],
                                              str(elt))

    def set_module_var(self):
        """
        set module variables from the config file
        """
        for elt in self.vnf_infos['list']:
            vf_config = {}

            # we cannot be sure that the modules are in teh same order
            # than the vnf
            vf_index = onap_utils.get_vf_module_index(
                self.module_infos['list'],
                elt)
            vnf_type = list(onap_utils.get_template_param(
                self.vnf_config["vnf"],
                "topology_template.groups"))[vf_index]
            self.__logger.debug("Get Module info for VNF %s", elt)
            vf_config["sdnc_vnf_type"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.groups." +
                vnf_type +
                ".metadata.vfModuleModelName")
            vf_config["vnf_parameters"] = \
                onap_utils.get_vnf_parameters(self.vnf_config["vnf"],
                                              str(elt))
            vf_config["module_invariant_id"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.groups." +
                vnf_type + ".metadata.vfModuleModelInvariantUUID")
            vf_config["module_name_version_id"] = (
                onap_utils.get_template_param(
                    self.vnf_config["vnf"], "topology_template.groups." +
                    vnf_type + ".metadata.vfModuleModelUUID"))
            vf_config["module_customization_id"] = (
                onap_utils.get_template_param(
                    self.vnf_config["vnf"], "topology_template.groups." +
                    vnf_type + ".metadata.vfModuleModelCustomizationUUID"))
            vf_config["module_version"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.groups." +
                vnf_type + ".metadata.vfModuleModelVersion")
            vf_config["module_version"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.groups." +
                vnf_type + ".metadata.vfModuleModelVersion")
            vf_config["module_version_id"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.groups." +
                vnf_type + ".metadata.vfModuleModelUUID")
            self.vnf_config[elt].update(vf_config)

    def set_onap_components(self):
        """
        Set ONAP component objects
        """
        self.components["aai"] = aai.Aai()
        self.components["so"] = so.So()
        self.components["sdnc"] = sdnc.Sdnc()
        self.components["nbi"] = nbi.Nbi(self.vnf_config["vnf"],
                                         self.vnf_config["sdnc_vnf_name"])

    def instantiate(self):
        """
        Instantiate a VNF with ONAP
          * Create the service instance (SO)
          * Create the VNF instance (SO)
          * preload the VNF in the SDNC
          * Create the VF module instance (SO)
        """
        instance_info = {"instance_id": ""}
        vnf_info = {"vnf_id": ""}
        module_info = {}
        module_ref = {"instanceId": ""}
        module_ok = False
        check_vnf = False
        self.__logger.info("Start the instantiation of the VNF")
        instance_info = self.create_service_instance()
        # as the VNF module is almost immediate
        # and does not require resource creation out of onap
        # we directly check the creation in the Aai
        # it could be improved by adding a check the so request status
        service_ok = self.components["aai"].check_service_instance(
            self.vnf_config["vnf_name"],
            instance_info["instance_id"])
        if service_ok:
            # create VNF instance(s)
            for elt in self.vnf_infos['list']:
                vnf_info = self.create_vnf_instance(elt)
                self.__logger.debug("Check vnf %s ", elt)
                vnf_ok = True
                # as the VNF module is almost immediate
                # and does not require resource creation out of onap
                # we directly check the creation in the Aai
                # it could be improved by adding a check the so request status
                if not self.components["aai"].check_vnf_instance(
                        vnf_info["vnf_id"]):
                    vnf_ok = False
                else:
                    # preload VNF(s) in SDNC
                    self.preload(instance_info["instance_id"], elt)
                time.sleep(10)
            if vnf_ok:
                # create VF module(s)
                for elt in self.vnf_infos['list']:
                    module_ok = True
                    try:
                        module_info = self.create_module_instance(
                            instance_info["instance_id"],
                            vnf_info["vnf_id"], elt)
                        module_ref = module_info['module_instance']

                        # check that the module has been created in the AAI
                        self.components["aai"].check_module_instance(
                            vnf_info["vnf_id"],
                            module_ref["requestReferences"]["instanceId"])
                        # check VNF using OpenStack client directly
                        check_vnf = self.check_vnf(
                            self.get_module_instance_name(elt))
                    except onap_test_exceptions.SoCompletionException:
                        module_ok = False
                        self.__logger.error(
                            "VF module not completed")
                    except onap_test_exceptions.SoRequestException:
                        module_ok = False
                        self.__logger.error(
                            "VF module request completed but KO")
                    except onap_test_exceptions.AaiRequestException:
                        module_ok = False
                        self.__logger.warning(
                            "VF module created but not seen in AAI")
        return {"status": module_ok,
                "instance_id": instance_info,
                "vnf_info": vnf_info,
                "module_info": module_info,
                "check_heat": check_vnf}

    def clean(self, **kwargs):
        """
        Clean VNF from ONAP

         Args:
            instance_id: The ID of the VNF service instance
            vnf_id: The ID of the VNF instance
            module_id: The ID of the VF module instance
        """
        if "instance_id" in kwargs:
            instance_id = kwargs['instance_id']
        else:
            instance_id = self.service_infos['instance_id']

        for elt in self.vnf_infos['list']:
            if "vnf_info" in kwargs:
                vnf_id = kwargs['vnf_info'][elt]["vnf_id"]
            else:
                vnf_id = self.vnf_infos[elt]["vnf_id"]

            if "module_info" in kwargs:
                module_id = kwargs['module_info'][elt]["module_id"]
            else:
                module_id = (self.module_infos[elt]["module_instance"]
                             ["requestReferences"]["instanceId"])
            self.clean_module(instance_id, vnf_id, module_id, elt)
            if not self.components["aai"].check_module_cleaned(vnf_id,
                                                               module_id):
                return False

            self.clean_vnf(instance_id, vnf_id, elt)
            if not self.components["aai"].check_vnf_cleaned(vnf_id):
                return False

            self.clean_instance(instance_id)
            if self.components["aai"].check_service_instance_cleaned(
                    self.vnf_config["vnf_name"], instance_id):
                self.__logger.debug("Instance still in AAI DB")
            else:
                return False
            time.sleep(5)
            self.clean_preload(elt)
        return True

    def create_service_instance(self):
        """
        Create service instance
        2 options to create the instance
          * with SO
          * with NBI
        """
        instance_id = None
        if self.vnf_config["nbi"]:
            self.__logger.info("1) Create Service instance from NBI")
            nbi_info = self.components["nbi"].create_service_order_nbi()
            self.__logger.debug("NBI infos: %s", nbi_info)
            time.sleep(5)
            instance_id = (
                self.components["nbi"].get_instance_id_from_order(
                    nbi_info["id"]))
        else:
            self.__logger.info("1) Create Service instance in SO")
            service_payload = self.get_service_payload()
            instance_id = self.components["so"].create_instance(
                service_payload)
        service_model_version = onap_utils.get_service_custom_config(
            self.vnf_config["vnf"], "version")
        service_instance_info = {"instance_id": instance_id,
                                 "model_version": service_model_version}
        self.__logger.debug("Service instance created: %s",
                            service_instance_info)
        self.service_infos = service_instance_info
        return service_instance_info

    def create_vnf_instance(self, elt):
        """
        Create VNF instance

        Args:
          * elt: the VNF
        """
        vnf_id = None
        self.__logger.info("2) Create VNF instance %s in SO", elt)

        vnf_payload = self.get_vnf_payload(
            self.service_infos["instance_id"], elt)

        vnf_id = self.components["so"].create_vnf(
            self.service_infos["instance_id"],
            vnf_payload)
        vnf_info = {"vnf_id": vnf_id,
                    "vnf_payload": vnf_payload}
        self.__logger.debug("SO vnf instance created %s", vnf_info)
        self.vnf_infos[elt] = vnf_info
        return vnf_info

    def preload(self, instance_id, elt):
        """
        Preload VNF in SDNC

        Args:
          * elt: the VNF
        """
        vnf_preload_infos = {}
        self.__logger.info("3) Preload VNF %s in SDNC", elt)

        sdnc_payload = self.get_sdnc_preload_payload(instance_id, elt)
        self.__logger.debug("SDNC preload payload %s", sdnc_payload)
        sdnc_preload = self.components["sdnc"].preload(sdnc_payload)
        self.__logger.debug("SDNC preload answer: %s", sdnc_preload)
        vnf_preload_infos[elt] = ({"sdnc_payload": sdnc_payload,
                                   "sdnc_preload": sdnc_preload})

        return vnf_preload_infos[elt]

    def create_module_instance(self, instance_id, vnf_id, elt):
        """
        Create module instance

        Args:
          * instance_info: dict including the instance_id, the request_info and
          the service payload
          * vnf_info: dict including the vnf_id, vnf_related_instance and the
          vnf payload
        """
        module_info = {}
        self.__logger.info("4) Create MODULE %s instance in SO", elt)

        module_payload = self.get_module_payload(instance_id, vnf_id, elt)

        self.__logger.debug("Module payload %s", module_payload)
        module_instance = self.components["so"].create_module(
            self.service_infos["instance_id"],
            self.vnf_infos[elt]["vnf_id"],
            module_payload)

        req_ref = module_instance["requestReferences"]["requestId"]
        # check the request status
        if self.components["so"].check_so_request_completed(req_ref):
            self.__logger.debug(
                "Module instance created: %s", module_instance)
            module_info = (
                {'module_instance': module_instance,
                 'module_payload': module_payload})
            self.__logger.info("SO module vf(s) created")
            self.__logger.debug("VF module(s) details: %s", module_info)
        else:
            self.__logger.error("SO module vf(s) not completed")
            module_info = None
        self.module_infos[elt] = module_info
        return module_info

    def check_vnf(self, stack_name):
        """
        Check VNF stack has been properly started
        """
        check_vnf = False
        if onap_utils.get_config("openstack.check"):
            self.__logger.debug("Openstack check enabled")
            my_stack_checker = sc.StackChecker()
            if my_stack_checker.check_stack_is_complete(stack_name):
                check_vnf = True
        else:
            self.__logger.debug("Openstack check disabled")
        return check_vnf

    def clean_instance(self, instance_id):
        """
        Clean VNF instance

        Args:
          * instance_id: The service instance of the VNF
        """
        self.__logger.info(" Clean Service Instance ")
        service_payload = self.get_service_payload()
        self.components["so"].delete_instance(instance_id, service_payload)

    def clean_vnf(self, instance_id, vnf_id, elt):
        """
        Clean  VNF

        Args:
          * instance_id: The service instance of the VNF
          * vnf_id:The VNF id of the VNF
        """
        self.__logger.info(" Clean vnf Instance %s ", elt)
        vnf_payload = self.get_vnf_payload(instance_id, elt)
        self.components["so"].delete_vnf(
            instance_id,
            vnf_id,
            vnf_payload)

    def clean_module(self, instance_id, vnf_id, module_id, elt):
        """
        Clean VNF Module

        Args:
          * instance_id: The service instance id of the VNF
          * vnf_id:The VNF id of the VNF
          * module_id: the VF module id of the VNF
        """
        self.__logger.info(" Clean Module VF Instance %s ", elt)
        module_payload = self.get_module_payload(instance_id, vnf_id, elt)
        self.components["so"].delete_module(
            module_payload,
            instance_id,
            vnf_id,
            module_id)

    def clean_preload(self, elt):
        """
        Clean VNF SDNC preload
        """
        self.__logger.info(" Clean Preload of %s ", elt)
        module_instance_name = self.get_module_instance_name(elt)
        # if 1 of the expected preload clean is FAIL we return False
        clean_preload = self.components["sdnc"].delete_preload(
            module_instance_name,
            self.vnf_config[elt]["sdnc_vnf_type"])
        return clean_preload

    def get_service_payload(self):
        model_info = self.components["so"].get_service_model_info(
            self.vnf_config['invariant_uuid'], self.vnf_config['uuid'])
        request_info = self.components["so"].get_request_info(
            self.vnf_config["vnf"] + "-service-instance-" +
            self.vnf_config['random_string'])
        return self.components["so"].get_service_payload(
            self.vnf_config["vnf"],
            request_info,
            model_info)

    def get_vnf_payload(self, instance_id, elt):
        model_info = self.components["so"].get_vnf_model_info(
            self.vnf_config[elt]['vnf_invariant_id'],
            self.vnf_config[elt]['vnf_version_id'],
            self.vnf_config[elt]['vnf_version'],
            self.vnf_config[elt]['vnf_model_name'],
            self.vnf_config[elt]['vnf_customization_id'],
            self.vnf_config[elt]['vnf_customization_name'])

        service_model_version = onap_utils.get_service_custom_config(
            self.vnf_config["vnf"], "version")

        vnf_related_instance = self.components["so"].get_vnf_related_instance(
            instance_id,
            self.vnf_config['invariant_uuid'],
            self.vnf_config['uuid'],
            service_model_version)

        vnf_instance_name = (self.vnf_config["vnf"] + "-vnf-instance-" +
                             str(elt).replace(" ", "_") + ("_") +
                             self.vnf_config['random_string'])

        request_info = self.components["so"].get_request_info(
            vnf_instance_name)

        return self.components["so"].get_vnf_payload(
            self.vnf_config["vnf"],
            request_info,
            model_info,
            vnf_related_instance)

    def get_sdnc_preload_payload(self, instance_id, elt):
        vnf_name = (self.vnf_config["vnf"] +
                    "-vfmodule-instance-" +
                    str(elt).replace(" ", "_") + "_" +
                    self.vnf_config['random_string'])

        vnf_instance_name = (self.vnf_config["vnf"] + "-vnf-instance-" +
                             str(elt).replace(" ", "_") + ("_") +
                             self.vnf_config['random_string'])

        vnf_topology_identifier = {
            "generic-vnf-name": vnf_instance_name,
            "generic-vnf-type": (
                self.vnf_config[elt]['vnf_generic_type']),
            "service-type": instance_id,
            "vnf-name": vnf_name,
            "vnf-type": self.vnf_config[elt]['sdnc_vnf_type']}
        return self.components["sdnc"].get_preload_payload(
            self.vnf_config[elt]['vnf_parameters'],
            vnf_topology_identifier)

    def get_module_payload(self, instance_id, vnf_id, elt):
        module_model_info = self.components["so"].get_module_model_info(
            self.vnf_config[elt]['module_invariant_id'],
            self.vnf_config[elt]['sdnc_vnf_type'],
            self.vnf_config[elt]['module_customization_id'],
            self.vnf_config[elt]['module_version_id'],
            self.vnf_config[elt]['module_version'])
        module_related_instance = (
            self.components["so"].get_module_related_instance(
                vnf_id,
                self.vnf_config[elt]['vnf_invariant_id'],
                self.vnf_config[elt]['vnf_version_id'],
                self.vnf_config[elt]['vnf_version'],
                self.vnf_config[elt]['vnf_model_name'],
                self.vnf_config[elt]['vnf_customization_id'],
                self.vnf_config[elt]['vnf_customization_name']))

        module_instance_name = self.get_module_instance_name(elt)

        request_info = self.components["so"].get_request_info(
            module_instance_name)

        vnf_instance_name = (self.vnf_config["vnf"] + "-vnf-instance-" +
                             str(elt).replace(" ", "_") + ("_") +
                             self.vnf_config['random_string'])

        vnf_related_instance = self.components["so"].get_vnf_related_instance(
            instance_id,
            self.vnf_config['invariant_uuid'],
            self.vnf_config['uuid'],
            vnf_instance_name)

        return self.components["so"].get_module_payload(
            self.vnf_config["vnf"],
            request_info,
            module_model_info,
            vnf_related_instance,
            module_related_instance)

    def get_module_instance_name(self, elt):
        return (self.vnf_config["vnf"] + "-vfmodule-instance-" +
                str(elt).replace(" ", "_") + "_" +
                self.vnf_config['random_string'])
