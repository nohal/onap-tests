#!/usr/bin/env python

# Copyright (c) 2017 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0

# pylint: disable=missing-docstring

import logging
import unittest
import os
import os.path

import onap_tests.utils.utils as onap_test_utils
import onap_tests.utils.exceptions as onap_test_exceptions

__author__ = "Morgan Richomme <morgan.richomme@orange.com>"


class UtilsTestingBase(unittest.TestCase):

    """The super class which testing classes could inherit."""

    logging.disable(logging.CRITICAL)

    _vfw_node1 = "vFWCL_vPKG-vf 0"
    _vfw_node2 = "vFWCL_vFWSNK-vf 0"
    _vfw_group1 = "vfwcl_vfwsnkvf0..VfwclVfwsnkVf..base_vfw..module-0"
    _vfw_group2 = "vfwcl_vpkgvf0..VfwclVpkgVf..base_vpkg..module-0"
    _vfw_list = [_vfw_node1, _vfw_node2]
    _vims_node = "Clearwater 0"
    _vims_group = "clearwater0..Clearwater..base_clearwater..module-0"
    _vims_list = [_vims_node]

    def setUp(self):
        pass

    def test_get_vf_module_index_fw(self):
        self.assertEqual(1, onap_test_utils.get_vf_module_index(
            self._vfw_list,
            self._vfw_group1))
        self.assertEqual(0, onap_test_utils.get_vf_module_index(
            self._vfw_list,
            self._vfw_group2))

    def test_get_vf_module_index_vims(self):
        self.assertEqual(0, onap_test_utils.get_vf_module_index(
            self._vims_list,
            self._vims_group))

    def test_get_vnf_parameters_foo(self):
        foo_param = [
            {"vnf-parameter-name": "public_net_id",
             "vnf-parameter-value": "2da53890-5b54-4d29-81f7-3185110636ed"},
            {"vnf-parameter-name": "key_name",
             "vnf-parameter-value": "cleouverte"},
            {"vnf-parameter-name": "security_group_name",
             "vnf-parameter-value": "basic-SSH-ICMP"},
            {"vnf-parameter-name": "flavor_name",
             "vnf-parameter-value": "onap.medium"},
            {"vnf-parameter-name": "VM_name",
             "vnf-parameter-value": "vFWCL_vPKG-vfmodule-instance-01"},
            {"vnf-parameter-name": "onap_private_net_id",
             "vnf-parameter-value": "2da53890-5b54-4d29-81f7-3185110636ed"},
            {"vnf-parameter-name": "onap_private_subnet_id",
             "vnf-parameter-value": "650a0971-7f01-4048-8424-69660eb57292"},
            {"vnf-parameter-name": "dcae_collector_ip",
             "vnf-parameter-value": "10.4.2.44"},
            {"vnf-parameter-name": "unprotected_private_net_id",
             "vnf-parameter-value": "46ef4db4-17ed-49ab-973e-0c8d5645f2d8"},
            {"vnf-parameter-name": "unprotected_private_subnet_id",
             "vnf-parameter-value": "69da1f58-ae4d-4901-bf70-bcba62b2909b"},
            {"vnf-parameter-name": "protected_private_net_id",
             "vnf-parameter-value": "0654c3af-9b1c-47f8-b000-d2188d27aecf"},
            {"vnf-parameter-name": "protected_private_subnet_id",
             "vnf-parameter-value": "73dbdb86-4461-4dba-b9e3-1da6fa052116"}]
        self.assertEqual(foo_param,
                         onap_test_utils.get_vnf_parameters("foo",
                                                            "vFWCL_vPKG_vnf"))

    def test_get_vnf_parameters_bad_vnf_type(self):
        self.assertEqual([],
                         onap_test_utils.get_vnf_parameters("bad_type",
                                                            "vFWCL_vPKG_vnf"))

    def test_get_vnf_parameters_bad_vnf_name(self):
        self.assertEqual(None,
                         onap_test_utils.get_vnf_parameters("foo",
                                                            "Bad name"))

    def test_get_vnf_instance_names_foo(self):
        self.assertEqual(["vFWCL_vPKG-vf 0", "vFWCL_vFWSNK-vf 0"],
                         onap_test_utils.get_vnf_instance_names("foo"))

    def test_get_vnf_instance_names_bad_service(self):
        self.assertEqual([],
                         onap_test_utils.get_vnf_instance_names("vbadservice"))

    def test_get_service_custom_config_with_param(self):
        self.assertEqual("1.0",
                         onap_test_utils.get_service_custom_config(
                             "foo",
                             "version"))

    def test_get_service_custom_config(self):
        foo_config = onap_test_utils.get_service_custom_config("foo")
        # tosca_file_from_SDC: service-ClearwaterVims-template
        # version: "1.0"
        self.assertEqual("service-FooService-template",
                         foo_config["tosca_file_from_SDC"])

    def test_create_zip_dir(self):
        # test the creation of a zip file for foo sample
        # 1) delete zip file if it exists
        root_path = os.getcwd().rsplit('/onap-tests')[0]
        foo_path = "/onap-tests/onap_tests/templates/heat_files/foo"
        filename = root_path + foo_path + "/foo.zip"
        try:
            os.remove(filename)
        except OSError:
            pass
        # 2) create zip
        onap_test_utils.create_zip_dir(filename)
        # 3) check the file has been created
        self.assertTrue(os.path.isfile(filename))
        # 4) delete zip
        try:
            os.remove(filename)
        except OSError:
            pass

    def test_create_zip_bad_dir(self):
        # test the creation of a zip file for foo sample
        # 1) delete zip file if it exists
        root_path = os.getcwd().rsplit('/onap-tests')[0]
        foo_path = "/onap-tests/onap_tests/templates/heat_files/foo2"
        filename = root_path + foo_path + "/foo.zip"
        try:
            os.remove(filename)
        except OSError:
            pass
        # 2) create zip and check the exception has been raised
        with self.assertRaises(onap_test_exceptions.SdcHeatZipException):
            onap_test_utils.create_zip_dir(filename)

    def test_create_zip_dir_zip_already_exists(self):
        root_path = os.getcwd().rsplit('/onap-tests')[0]
        foo_path = "/onap-tests/onap_tests/templates/heat_files/foo"
        filename = root_path + foo_path + "/foo.zip"
        # 1) create it once
        onap_test_utils.create_zip_dir(filename)
        # 2) check the file has been created
        self.assertTrue(os.path.isfile(filename))
        # 3) recreate it and check there is no issue
        onap_test_utils.create_zip_dir(filename)
        self.assertTrue(os.path.isfile(filename))
        # 4) delete zip
        try:
            os.remove(filename)
        except OSError:
            pass

if __name__ == "__main__":
    # logging must be disabled else it calls time.time()
    # what will break these unit tests.
    logging.disable(logging.CRITICAL)
    unittest.main(verbosity=2)
