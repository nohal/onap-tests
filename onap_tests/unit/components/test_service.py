#!/usr/bin/env python

# Copyright (c) 2017 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0

# pylint: disable=missing-docstring

import logging
import unittest

from onap_tests.components import service


class SdcTestingBase(unittest.TestCase):

    __logger = logging.getLogger(__name__)

    def setUp(self):
        self.my_service = service.Service()

    def test_get_sdc_headers(self):
        old_key = "USER_ID"  # does exist in headers by default
        new_key = "user_id"
        new_value = "op001"
        transformed_headers = self.my_service.get_sdc_headers(
            old_key, new_key, new_value)
        self.assertEqual(transformed_headers[new_key], new_value)
        with self.assertRaises(KeyError):
            transformed_headers[old_key]

    def test_get_sdc_headers_unknown_field(self):
        old_key = "tata"  # does not exist in headers by default
        new_key = "toto"
        new_value = "titi"
        transformed_headers = self.my_service.get_sdc_headers(
            old_key, new_key, new_value)
        self.assertEqual(transformed_headers[new_key], new_value)
        with self.assertRaises(KeyError):
            transformed_headers[old_key]


if __name__ == "__main__":
    # logging must be disabled else it calls time.time()
    # what will break these unit tests.
    logging.disable(logging.CRITICAL)
    unittest.main(verbosity=2)
